// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package main // import "gitlab.com/tromos/tromos-ce/cmd/tromos-daemon"

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"os"
	"os/signal"
)

func init() {
	logrus.SetFormatter(new(logrus.JSONFormatter))
	// initial log formatting; this setting is updated after the daemon configuration is loaded.
}

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:                   "tromosd [OPTIONS]",
	Short:                 "Tromos standalone command line tool",
	SilenceUsage:          true,
	DisableFlagsInUseLine: true,
	// Uncomment the following line if your bare application
	// has an action associated with it:
}

func main() {

	startCmd, stopCmd, err := newDaemonCommands()
	if err != nil {
		logrus.Fatal(err)
	}

	// When the daemon is running on the foreground, the user can
	// terminate it with ctrl+c. When running on the background,
	// the user can terminate the daemon through daemon stop command
	errChan := make(chan error, 1)
	go func() {
		defer close(errChan)
		if err := startCmd.Execute(); err != nil {
			errChan <- err
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	// Block waiting either an error to occur to a termination signal to come
	select {
	case <-c:
		signal.Reset(os.Interrupt)
		if err := stopCmd.Execute(); err != nil {
			logrus.Fatal("Daemon forcibly terminated")
			return
		}

	case err := <-errChan:
		if err != nil {
			return
		}
	}
}
