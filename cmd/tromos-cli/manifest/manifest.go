// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package manifest // import "gitlab.com/tromos/tromos-ce/cmd/tromos-cli/manifest"

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/engine/manifest"
)

func init() {
	Cmd.AddCommand(validateCmd)
	validateCmd.Flags().String("manifest", "", "Location to manifest")
}

var Cmd = &cobra.Command{
	Use:   "manifest",
	Short: "manifest related operations",
	Long:  `Handle all the operations related to a container manifest`,
}

var validateCmd = &cobra.Command{
	Use:   "validate",
	Short: "validate the manifest",
	RunE: func(cmd *cobra.Command, args []string) error {
		flags := cmd.Flags()
		if err := viper.BindPFlag("manifest", flags.Lookup("manifest")); err != nil {
			return err
		}

		localmanifest := viper.New()

		// Load a insitu manifest
		localmanifest.SetConfigFile(viper.GetString("manifest"))
		if err := localmanifest.ReadInConfig(); err != nil {
			return err
		}

		man, err := manifest.NewManifest(localmanifest)
		if err != nil {
			return err
		}

		if err := man.Validate(); err != nil {
			return err
		}
		return nil
	},
}
