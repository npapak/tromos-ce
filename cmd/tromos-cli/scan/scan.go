// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package scan // import "gitlab.com/tromos/tromos-ce/cmd/tromos-cli/scan"

/*
import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/engine/middleware"
	"log"
)

func init() {
	cli.RootCmd.AddCommand(scanCmd)

	flags := startCmd.Flags()
	flags.String("manifest", "", "Execute from manifest")
	if err := scanCmd.MarkFlagRequired("manifest"); err != nil {
		panic(err)
	}

	flags.String("fromdevice", "", "Device to import data")
	if err := scanCmd.MarkFlagRequired("fromdevice"); err != nil {
		panic(err)
	}
}

var scanCmd = &cobra.Command{
	Use:   "scan",
	Short: "Index the contents of existing systems",
	Long:  "Index the contents of existing systems",
	RunE: func(cmd *cobra.Command, args []string) error {

		localmanifest := viper.New()
		localmanifest.SetConfigFile(viper.GetString("manifest"))
		if err := localmanifest.ReadInConfig(); err != nil {
			return err
		}

		man, err := manifest.NewManifest(localmanifest)
		if err != nil {
			return err
		}

		client, err := middleware.NewClient(middleware.ClientConfig{
			Manifest: man,
		})
		if err != nil {
			return err
		}
		defer client.Close()

		deviceid, err := cmd.Flags().GetString("fromdevice")
		if err != nil {
			log.Fatal(err)
		}

		if err := client.ImportFrom(deviceid); err != nil {
			log.Fatal(err)
		}
	},
}
*/
