module gitlab.com/tromos/tromos-ce

require (
	bazil.org/fuse v0.0.0-20180421153158-65cc252bf669
	code.cloudfoundry.org/bytefmt v0.0.0-20190710193110-1eb035ffe2b6
	github.com/DataDog/zstd v1.4.0
	github.com/aybabtme/iocontrol v0.0.0-20150809002002-ad15bcfc95a0
	github.com/benbjohnson/clock v0.0.0-20161215174838-7dc76406b6d3 // indirect
	github.com/boltdb/bolt v1.3.1
	github.com/davecgh/go-spew v1.1.1
	github.com/djherbis/stream v1.2.0
	github.com/go-playground/locales v0.12.1 // indirect
	github.com/go-playground/universal-translator v0.16.0 // indirect
	github.com/golang/groupcache v0.0.0-20190129154638-5b532d6fd5ef
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.2
	github.com/hashicorp/go-getter v1.3.0
	github.com/hashicorp/go-immutable-radix v1.1.0
	github.com/hashicorp/yamux v0.0.0-20181012175058-2f1d1f20f75d
	github.com/hprose/hprose-golang v2.0.4+incompatible
	github.com/jolestar/go-commons-pool v2.0.0+incompatible
	github.com/juju/ratelimit v1.0.1
	github.com/klauspost/dedup v1.1.0
	github.com/klauspost/reedsolomon v1.9.2
	github.com/leodido/go-urn v1.1.0 // indirect
	github.com/mattn/goveralls v0.0.2 // indirect
	github.com/miolini/datacounter v0.0.0-20171104152933-fd4e42a1d5e0
	github.com/orcaman/concurrent-map v0.0.0-20190314100340-2693aad1ed75
	github.com/pierrre/gotestcover v0.0.0-20160517101806-924dca7d15f0 // indirect
	github.com/prasmussen/gdrive v0.0.0-20190419185059-29ca5a922a95
	github.com/sirupsen/logrus v1.4.2
	github.com/spacemonkeygo/errors v0.0.0-20171212215202-9064522e9fd1
	github.com/spf13/afero v1.2.2
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.4.0
	github.com/stretchr/testify v1.3.0
	github.com/trustmaster/goflow v0.0.0-20180414123758-47a1b442f390
	github.com/workiva/go-datastructures v1.0.50
	gitlab.com/tromos/hub v0.0.0-20190727190606-aa8ccb55793b
	golang.org/x/net v0.0.0-20190613194153-d28f0bde5980
	golang.org/x/oauth2 v0.0.0-20190604053449-0f29369cfe45
	gonum.org/v1/plot v0.0.0-20190615073203-9aa86143727f
	google.golang.org/api v0.7.0
	gopkg.in/cheggaaa/pb.v1 v1.0.28
	gopkg.in/go-playground/validator.v9 v9.29.0
)
