// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package coordinator // import "gitlab.com/tromos/tromos-ce/engine/peer/coordinator"

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/tromos/hub/coordinator"
	"gitlab.com/tromos/hub/selector"
	"gitlab.com/tromos/tromos-ce/engine/hub"
	"gitlab.com/tromos/tromos-ce/pkg/net"
	"gitlab.com/tromos/tromos-ce/pkg/structures"
	"gopkg.in/go-playground/validator.v9"
)

type CoordinatorConfig struct {
	ID         string        `validate:"required"`
	Parameters *viper.Viper  `validate:"required"`
	Logger     *logrus.Entry `validate:"required"`
}

// Coordinator is a logical Microservice composed out of several coordinator.Coordinator
// processing layers
type Coordinator struct {
	coordinator.Coordinator
	config       *CoordinatorConfig
	capabilities []selector.Capability
}

func (c *Coordinator) String() string {
	return c.config.ID
}

func (c *Coordinator) Capabilities() []selector.Capability {
	return c.capabilities
}

// New returns a new Coordinator
func New(config CoordinatorConfig) (*Coordinator, error) {

	validate := validator.New()
	if err := validate.Struct(config); err != nil {
		return nil, coordinator.ErrInvalid
	}

	var layer coordinator.Coordinator
	islocal := net.IsLocalAddress(config.Parameters.GetString("Proxy.host"))

	// It is insitu when 1) there is no proxy 2) the proxy ip is a insitu ip
	if !islocal {
		proxyclient := config.Parameters.Sub("proxy")
		plugin, err := hub.OpenCoordinatorPlugin(proxyclient.GetString("plugin") + "/client")
		if err != nil {
			return nil, err
		}
		layer, err = plugin(proxyclient)
		if err != nil {
			return nil, err
		}

		layer.SetBackend(coordinator.DefaultCoordinator{})
	} else {
		persistent := config.Parameters.Sub("persistent")

		plugin, err := hub.OpenCoordinatorPlugin(persistent.GetString("plugin"))
		if err != nil {
			return nil, err
		}

		layer, err = plugin(persistent)
		if err != nil {
			return nil, err
		}

		layer.SetBackend(coordinator.DefaultCoordinator{})

		// Load intermediate connectors (and change high-order accoringdly)
		stack := structures.SortMapStringKeys(config.Parameters.GetStringMap("Translators"))
		for _, seqID := range stack {
			translator := config.Parameters.Sub("translators." + seqID)

			plugin, err := hub.OpenCoordinatorPlugin(translator.GetString("plugin"))
			if err != nil {
				return nil, err
			}

			newlayer, err := plugin(translator)
			if err != nil {
				return nil, err
			}

			newlayer.SetBackend(layer)
			layer = newlayer
		}

		// Local coordinator exposed through proxy (the proxy ip is the insitu ip)
		if len(config.Parameters.GetStringMapString("Proxy")) > 0 {
			proxyserver := config.Parameters.Sub("proxy")
			plugin, err := hub.OpenCoordinatorPlugin(proxyserver.GetString("plugin") + "/server")
			if err != nil {
				return nil, err
			}

			server, err := plugin(proxyserver)
			if err != nil {
				return nil, err
			}

			server.SetBackend(layer)
		}
	}

	// Convert the capability strings to variables
	capabilities := []selector.Capability{selector.Default}
	for _, capability := range viper.GetStringSlice("Capabilities") {
		capability, ok := selector.Capabilities[capability]
		if !ok {
			return nil, coordinator.ErrCapability
		}
		capabilities = append(capabilities, capability)
	}

	c := &Coordinator{
		Coordinator:  layer,
		config:       &config,
		capabilities: capabilities,
	}

	config.Logger.Infof("Found Coordinator %s @ % s", c.String(), c.Location())
	return c, nil
}
