// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package coordinator // import "gitlab.com/tromos/tromos-ce/engine/peer/coordinator"

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"gitlab.com/tromos/hub/coordinator"
	"gitlab.com/tromos/tromos-ce/engine/hub"
	"gitlab.com/tromos/tromos-ce/engine/manifest"
	"gitlab.com/tromos/tromos-ce/pkg/debug"
	"testing"
)

var (
	Peer     = "127.0.0.1"
	Manifest = "./coordinator_test.yml"

	coordinators []*Coordinator
	mani         *manifest.Manifest
)

func init() {
	viper.SetConfigFile(Manifest)
	if err := viper.ReadInConfig(); err != nil {
		panic(err)
	}

	man, err := manifest.NewManifest(viper.GetViper())
	if err != nil {
		panic(err)
	}
	mani = man

	// Find and download all the plugins for the peer
	pluginList := man.PluginsOnPeer(Peer)
	if len(pluginList) == 0 {
		panic("No plugins were defined")
	}

	// Download all the necessary plugins
	if err := hub.FixDependencies(pluginList); err != nil {
		panic(err)
	}

	// Set logger preferences
	logrus.SetLevel(logrus.DebugLevel)
}

func TestNew(t *testing.T) {
	_, err := New(CoordinatorConfig{})
	assert.Equal(t, err, coordinator.ErrInvalid, "Return error should be empty")

	// Register all the devices
	for _, coordid := range mani.Coordinators() {
		logger := logrus.WithFields(logrus.Fields{
			"Coordinator": coordid,
			"File":        debug.WhereAmI(),
		})

		config := CoordinatorConfig{
			ID:         coordid,
			Parameters: mani.CoordinatorConfiguration(coordid),
			Logger:     logger,
		}

		coord, err := New(config)
		assert.Nil(t, err)
		coordinators = append(coordinators, coord)
	}
}
