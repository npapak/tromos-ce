// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package device // import "gitlab.com/tromos/tromos-ce/engine/peer/device"

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/tromos/hub/device"
	"gitlab.com/tromos/hub/selector"
	"gitlab.com/tromos/tromos-ce/engine/hub"
	"gitlab.com/tromos/tromos-ce/pkg/net"
	"gitlab.com/tromos/tromos-ce/pkg/structures"
	"gopkg.in/go-playground/validator.v9"
)

type DeviceConfig struct {
	ID         string        `validate:"required"`
	Parameters *viper.Viper  `validate:"required"`
	Logger     *logrus.Entry `validate:"required"`
}

// Device is a logical Microservice composed out of several device.Device processing layers
type Device struct {
	device.Device
	config       *DeviceConfig
	capabilities []selector.Capability
}

func (d *Device) String() string {
	return d.config.ID
}

func (d *Device) Capabilities() []selector.Capability {
	return d.capabilities
}

// New returns a new Device
func New(config DeviceConfig) (*Device, error) {

	validate := validator.New()
	if err := validate.Struct(config); err != nil {
		return nil, device.ErrInvalid
	}

	var layer device.Device
	islocal := net.IsLocalAddress(config.Parameters.GetString("Proxy.host"))

	// It is insitu when 1) there is no proxy 2) the proxy ip is a insitu ip
	if !islocal {
		proxyclient := config.Parameters.Sub("proxy")
		plugin, err := hub.OpenDevicePlugin(proxyclient.GetString("plugin") + "/client")
		if err != nil {
			return nil, err
		}

		layer = plugin(proxyclient)
		layer.SetBackend(device.DefaultDevice{})
	} else {
		persistent := config.Parameters.Sub("persistent")
		plugin, err := hub.OpenDevicePlugin(persistent.GetString("plugin"))
		if err != nil {
			return nil, err
		}

		layer = plugin(persistent)
		layer.SetBackend(device.DefaultDevice{})

		// Load intermediate connectors (and change high-order accoringdly)
		stack := structures.SortMapStringKeys(config.Parameters.GetStringMap("translators"))
		for _, seqID := range stack {
			translator := config.Parameters.Sub("translators." + seqID)
			plugin, err := hub.OpenDevicePlugin(translator.GetString("plugin"))
			if err != nil {
				return nil, err
			}

			newlayer := plugin(translator)
			newlayer.SetBackend(layer)
			layer = newlayer
		}

		// Local device exposed through proxy (the proxy ip is the insitu ip)
		if len(config.Parameters.GetStringMapString("Proxy")) > 0 {
			proxyserver := config.Parameters.Sub("proxy")
			plugin, err := hub.OpenDevicePlugin(proxyserver.GetString("plugin") + "/server")
			if err != nil {
				return nil, err
			}

			server := plugin(proxyserver)
			server.SetBackend(layer)
		}
	}

	// Convert the capability strings to variables
	capabilities := []selector.Capability{selector.Default}
	for _, capacity := range viper.GetStringSlice("Capabilities") {
		capability, ok := selector.Capabilities[capacity]
		if !ok {
			return nil, device.ErrCapability
		}
		capabilities = append(capabilities, capability)
	}

	d := &Device{
		Device:       layer,
		config:       &config,
		capabilities: capabilities,
	}

	config.Logger.Infof("Found Device %s @ % s", d.String(), d.Location())
	return d, nil
}
