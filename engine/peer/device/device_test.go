// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package device // import "gitlab.com/tromos/tromos-ce/engine/peer/device"

import (
	"bytes"
	"code.cloudfoundry.org/bytefmt"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"gitlab.com/tromos/hub/device"
	"gitlab.com/tromos/tromos-ce/engine/hub"
	"gitlab.com/tromos/tromos-ce/engine/manifest"
	"gitlab.com/tromos/tromos-ce/pkg/debug"
	"gitlab.com/tromos/tromos-ce/pkg/uuid"
	"io"
	"testing"
)

var (
	Peer     = "127.0.0.1"
	Filesize = "1M"
	Manifest = "./device_test.yml"

	MockingData []byte
	devices     []*Device
	mani        *manifest.Manifest
)

func init() {
	viper.SetConfigFile(Manifest)
	if err := viper.ReadInConfig(); err != nil {
		panic(err)
	}

	man, err := manifest.NewManifest(viper.GetViper())
	if err != nil {
		panic(err)
	}
	mani = man

	// Find and download all the plugins for the peer
	pluginList := man.PluginsOnPeer(Peer)
	if len(pluginList) == 0 {
		panic("No plugins were defined")
	}

	// Download all the necessary plugins
	if err := hub.FixDependencies(pluginList); err != nil {
		panic(err)
	}

	filesize, err := bytefmt.ToBytes(Filesize)
	if err != nil {
		panic(err)
	}

	// Dummy data
	MockingData = make([]byte, filesize)

	// Set logger preferences
	logrus.SetLevel(logrus.DebugLevel)
}

func TestNew(t *testing.T) {
	_, err := New(DeviceConfig{})
	assert.Equal(t, err, device.ErrInvalid, "Return error should be empty")

	// Register all the devices
	for _, devid := range mani.Devices() {
		logger := logrus.WithFields(logrus.Fields{
			"Device": devid,
			"File":   debug.WhereAmI(),
		})

		config := DeviceConfig{
			ID:         devid,
			Parameters: mani.DeviceConfiguration(devid),
			Logger:     logger,
		}

		dev, err := New(config)
		assert.Nil(t, err)
		devices = append(devices, dev)
	}
}

func TestTransfer(t *testing.T) {
	for i := 0; i < len(devices); i++ {
		t.Run(devices[i].String(), testSequentialWrites(devices[i]))
	}

	for i := 0; i < len(devices); i++ {
		t.Run(devices[i].String(), testParallelWrites(devices[i]))
	}
}

func testSequentialWrites(dev device.Device) func(t *testing.T) {
	return func(t *testing.T) {
		err := sequentialWrite(dev, 1)
		assert.Nil(t, err)
	}
}

func sequentialWrite(dev device.Device, factor int) error {
	chw, err := dev.NewWriteChannel(uuid.Once())
	if err != nil {
		return err
	}

	for i := 0; i < 3*factor; i++ {
		pr, pw := io.Pipe()
		if err := chw.NewTransfer(pr, &device.Stream{Complete: make(chan struct{})}); err != nil {
			return err
		}

		if _, err = io.Copy(pw, bytes.NewReader(MockingData)); err != nil {
			return err
		}

		if _, err := io.Copy(pw, bytes.NewReader(MockingData)); err != nil {
			return err
		}

		if err := pw.Close(); err != nil {
			return err
		}
	}
	if err := chw.Close(); err != nil {
		return err
	}

	return nil
}

func testParallelWrites(dev *Device) func(t *testing.T) {
	return func(t *testing.T) {
		err := parallelWrite(dev, 1)
		assert.Nil(t, err)
	}
}

func parallelWrite(dev *Device, factor int) error {
	chw, err := dev.NewWriteChannel(uuid.Once())
	if err != nil {
		return err
	}

	for i := 0; i < 3*factor; i++ {

		pr, pw := io.Pipe()
		if err := chw.NewTransfer(pr, &device.Stream{Complete: make(chan struct{})}); err != nil {
			return err
		}

		go func() {
			if _, err := io.Copy(pw, bytes.NewReader(MockingData)); err != nil {
				panic(err)
			}

			if _, err := io.Copy(pw, bytes.NewReader(MockingData)); err != nil {
				panic(err)
			}

			if err := pw.Close(); err != nil {
				panic(err)
			}
		}()
	}

	if err := chw.Close(); err != nil {
		return err
	}
	return nil
}

func BenchmarkSequentialDev0(b *testing.B) {
	err := sequentialWrite(devices[0], b.N)
	if err != nil {
		panic(err)
	}
}

func BenchmarkSequentialDev1(b *testing.B) {
	err := sequentialWrite(devices[1], b.N)
	if err != nil {
		panic(err)
	}

}
func BenchmarkSequentialDev2(b *testing.B) {
	err := sequentialWrite(devices[2], b.N)
	if err != nil {
		panic(err)
	}
}

func BenchmarkSequentialDev3(b *testing.B) {
	err := sequentialWrite(devices[3], b.N)
	if err != nil {
		panic(err)
	}
}

func BenchmarkSequentialDev4(b *testing.B) {
	err := sequentialWrite(devices[4], b.N)
	if err != nil {
		panic(err)
	}
}

func BenchmarkSequentialDev5(b *testing.B) {
	err := sequentialWrite(devices[5], b.N)
	if err != nil {
		panic(err)
	}
}

func BenchmarkParallelDev0(b *testing.B) {
	err := parallelWrite(devices[0], b.N)
	if err != nil {
		panic(err)
	}
}

func BenchmarkParallelDev1(b *testing.B) {
	err := parallelWrite(devices[1], b.N)
	if err != nil {
		panic(err)
	}
}
func BenchmarkParallelDev2(b *testing.B) {
	err := parallelWrite(devices[2], b.N)
	if err != nil {
		panic(err)
	}
}

func BenchmarkParallelDev3(b *testing.B) {
	err := parallelWrite(devices[3], b.N)
	if err != nil {
		panic(err)
	}
}

func BenchmarkParallelDev4(b *testing.B) {
	err := parallelWrite(devices[4], b.N)
	if err != nil {
		panic(err)
	}
}

func BenchmarkParallelDev5(b *testing.B) {
	err := parallelWrite(devices[5], b.N)
	if err != nil {
		panic(err)
	}
}
