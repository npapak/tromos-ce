// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package peer // import "gitlab.com/tromos/tromos-ce/engine/peer"

import (
	rpc "github.com/hprose/hprose-golang/rpc/websocket"
	"net/http"
)

// RunWebservice makes the peer functionality availablle through RPC
func (peer *Peer) RunWebservice() error {
	ops := rpc.NewWebSocketService()
	ops.AddFunction("Bootstrap", peer.Bootstrap)

	webservice := &http.Server{
		Addr:    peer.address + ":" + WebServicePort,
		Handler: ops,
	}
	peer.webservice = webservice

	errCh := make(chan error)

	go func() {
		peer.logger.Printf("Starting Webservice at %s", webservice.Addr)
		err := webservice.ListenAndServe()
		errCh <- err
		close(errCh)
	}()
	return <-errCh
}
