// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package processor // import "gitlab.com/tromos/tromos-ce/engine/peer/processor"

import (
	"context"
	"github.com/sirupsen/logrus"
	"github.com/trustmaster/goflow"
	"gitlab.com/tromos/hub/processor"
	"gitlab.com/tromos/hub/selector"
	//"gitlab.com/tromos/tromos-ce/pkg/log"
	"reflect"
)

type Builder struct {
	logger    *logrus.Entry
	processor *Processor

	// Internal network
	network       *flow.Graph
	writable      bool
	capabilitymap map[string][]selector.Capability // port -> key -> value

	// External network
	isActive       chan struct{}
	ctxWithCancel  context.Context
	cancelFunction context.CancelFunc

	inport     chan processor.Stream // Input to the process network
	outports   []string              // Port identifiers
	readyports []reflect.SelectCase  // Ports ready to send/receive data
}

func (p *Processor) newBuilder(writable bool, graph processor.ProcessGraph) *Builder {

	network := new(flow.Graph)
	network.InitGraphState()

	ctx, cancel := context.WithCancel(context.Background())

	b := &Builder{
		logger:    logrus.WithField("module", "processor/builder"),
		processor: p,

		network:       network,
		writable:      writable,
		capabilitymap: make(map[string][]selector.Capability),

		ctxWithCancel:  ctx,
		cancelFunction: cancel,
		isActive:       make(chan struct{}),
	}

	// populate the blank network with components
	if writable {
		graph.Upstream(b)
	} else {
		graph.Downstream(b)
	}
	// and instantiate it

	b.inport = make(chan processor.Stream)
	b.network.SetInPort("In", b.inport)

	outportsV := reflect.ValueOf(*b.network).FieldByName("outPorts").MapKeys()
	b.outports = make([]string, len(outportsV))
	for i := 0; i < len(outportsV); i++ {
		b.outports[i] = outportsV[i].String()
	}

	// Create asynchronous notification channels for the network output
	// ports to signal processor sinks when they have data
	b.readyports = make([]reflect.SelectCase, len(b.outports))
	for i := 0; i < len(b.outports); i++ {

		// Set notification channel for ports that have data
		ch := make(chan processor.Stream)
		b.network.SetOutPort(b.outports[i], ch)
		b.readyports[i] = reflect.SelectCase{
			Dir:  reflect.SelectRecv,
			Chan: reflect.ValueOf(ch),
		}
	}

	// Wait until the net has completed its job
	flow.RunNet(b.network)
	<-b.network.Ready()

	return b
}

func (b *Builder) reset() {
	// Terminate outputs of the Channel's instance, But do not
	// close the graph network
	b.cancelFunction()

	// Wait for the completion of cancel function
	<-b.isActive

	ctx, cancel := context.WithCancel(context.Background())
	b.ctxWithCancel = ctx
	b.cancelFunction = cancel
	b.isActive = make(chan struct{})
}

func (b *Builder) Close() error {

	// If applicable, return the builder object to the pool
	if b.writable && b.processor.upstreams.pool != nil {
		b.reset()
		return b.processor.upstreams.pool.ReturnObject(context.Background(), b)
	}
	if !b.writable && b.processor.downstreams.pool != nil {
		b.reset()
		return b.processor.downstreams.pool.ReturnObject(context.Background(), b)
	}

	// Wait for the internal network to gracefully close
	close(b.inport)
	<-b.network.Wait()

	// Propagate termination to Processor's outputs
	b.cancelFunction()

	return nil
}

func (b *Builder) Add(name string, module interface{}) bool {
	return b.network.Add(module, name)
}

func (b *Builder) Connect(senderName, senderPort, receiverName, receiverPort string) bool {
	return b.network.Connect(senderName, senderPort, receiverName, receiverPort)
}

func (b *Builder) MapInPort(port, app, appPort string) {
	if !b.network.MapInPort(port, app, appPort) {
		panic("MapInPort failed")
	}
}

func (b *Builder) MapOutPort(port, app, appPort string, annotations ...selector.Capability) {
	b.network.MapOutPort(port, app, appPort)
	b.capabilitymap[port] = annotations
}

func (b *Builder) Capabilities(port string) []selector.Capability {
	return b.capabilitymap[port]
}

func (b *Builder) NewTransfer(stream processor.Stream) error {
	b.inport <- stream

	return nil
}

func (b *Builder) Sinks() map[string]string {
	sinks := make(map[string]string)
	for _, port := range b.outports {
		sinks[port] = ""
	}
	return sinks
}

func (b *Builder) Readyports() <-chan processor.Stream {

	events := make(chan processor.Stream)
	go func(events chan processor.Stream) {
		for {
			select {
			case <-b.ctxWithCancel.Done():
				// Close events and propagate termination to the outputs
				close(events)
				// Close this channel and acknowledge termination
				close(b.isActive)
				return
			default:
				chosen, value, _ := reflect.Select(b.readyports)

				substream := value.Interface().(processor.Stream)
				// This may happen for modules that on donwlink do not wish to consume
				// data from all the connected sinks (e.g., mirror)
				if (processor.Stream{}) == substream {
					continue
				}
				substream.Port = b.outports[chosen]
				events <- substream
			}
		}
	}(events)
	return events
}
