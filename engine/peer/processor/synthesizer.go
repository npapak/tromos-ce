// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package processor // import "gitlab.com/tromos/tromos-ce/engine/peer/processor"

import (
	"context"
	"github.com/jolestar/go-commons-pool"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/tromos/hub/processor"
	"gitlab.com/tromos/hub/selector"
	"gitlab.com/tromos/tromos-ce/configuration"
	"gitlab.com/tromos/tromos-ce/engine/hub"
	"gopkg.in/go-playground/validator.v9"
)

type upstream struct {
	pool     *pool.ObjectPool
	generate func() *Builder
}

type downstream struct {
	pool     *pool.ObjectPool
	generate func() *Builder
}

type ProcessorConfig struct {
	ID         string        `validate:"required"`
	Parameters *viper.Viper  `validate:"required"`
	Logger     *logrus.Entry `validate:"required"`
}

type Processor struct {
	config *ProcessorConfig

	peer         string
	capabilities []selector.Capability
	upstreams    upstream
	downstreams  downstream
}

// New returns a new Processor
func New(config ProcessorConfig) (*Processor, error) {
	validate := validator.New()
	if err := validate.Struct(config); err != nil {
		return nil, processor.ErrInvalid
	}

	graphconfig := config.Parameters.Sub("graph")
	plugin, err := hub.OpenProcessorPlugin(graphconfig.GetString("plugin"))
	if err != nil {
		return nil, err
	}

	graph := plugin(graphconfig)

	// Convert the capability strings to variables
	capabilities := []selector.Capability{selector.Default}
	for _, capability := range viper.GetStringSlice("Capabilities") {
		capability, ok := selector.Capabilities[capability]
		if !ok {
			return nil, processor.ErrCapability
		}
		capabilities = append(capabilities, capability)
	}

	p := &Processor{
		peer:         "localhost",
		config:       &config,
		capabilities: capabilities,
	}
	p.init(graph)

	logrus.Infof("Found Processor %s @ %s", p.String(), p.Location())
	return p, nil
}

func (p *Processor) init(graph processor.ProcessGraph) {
	// function for compile at runtime
	p.upstreams.generate = func() *Builder {
		return p.newBuilder(true, graph)
	}
	p.downstreams.generate = func() *Builder {
		return p.newBuilder(false, graph)
	}

	// If not reusable, we cannot use pool
	if !graph.Reusable() {
		return
	}

	// generic pool configuration
	uctx := context.Background()
	config := &pool.ObjectPoolConfig{
		LIFO:                 true,
		MaxTotal:             configuration.MaxConcurrentChannels,
		MaxIdle:              -1,
		MinEvictableIdleTime: -1,
		BlockWhenExhausted:   false,
	}

	// Set up pools specifics
	p.upstreams.pool = pool.NewObjectPool(uctx,
		pool.NewPooledObjectFactorySimple(func(context.Context) (interface{}, error) {
			return p.upstreams.generate(), nil
		}),
		config,
	)
	pool.Prefill(uctx, p.upstreams.pool, configuration.MaxConcurrentChannels)

	p.downstreams.pool = pool.NewObjectPool(uctx,
		pool.NewPooledObjectFactorySimple(func(context.Context) (interface{}, error) {
			return p.downstreams.generate(), nil
		}),
		config,
	)
	pool.Prefill(uctx, p.downstreams.pool, configuration.MaxConcurrentChannels)

	// And make the generate to get items frm the pool
	p.upstreams.generate = func() *Builder {
		obj, err := p.upstreams.pool.BorrowObject(uctx)
		if err != nil {
			panic(err)
		}
		return obj.(*Builder)
	}

	p.downstreams.generate = func() *Builder {
		obj, err := p.downstreams.pool.BorrowObject(uctx)
		if err != nil {
			panic(err)
		}
		return obj.(*Builder)
	}
}

func (p *Processor) String() string {
	return p.config.ID
}

func (p *Processor) Location() string {
	return p.peer
}

func (p *Processor) Capabilities() []selector.Capability {
	return p.capabilities
}

func (p *Processor) NewChannel(opts ...processor.ChannelOption) (processor.Channel, error) {
	config := processor.ChannelConfig{}
	for _, opt := range opts {
		opt(&config)
	}
	return p.NewChannelFromConfig(config)
}

func (p *Processor) NewChannelFromConfig(config processor.ChannelConfig) (processor.Channel, error) {
	if config.Writable {
		return p.upstreams.generate(), nil
	} else {
		return p.downstreams.generate(), nil
	}
}

func (p *Processor) Close() error {
	// Close the pools if they exist. They may not exist though if
	// the graph is not reusable
	if p.upstreams.pool != nil {
		p.upstreams.pool.Close(context.Background())
	}
	if p.downstreams.pool != nil {
		p.downstreams.pool.Close(context.Background())
	}
	p.config.Logger.Infof("Processor has been successfully closed")
	return nil
}
