// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package processor // import "gitlab.com/tromos/tromos-ce/engine/peer/processor"

import (
	"bytes"
	"code.cloudfoundry.org/bytefmt"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"gitlab.com/tromos/hub/device"
	"gitlab.com/tromos/hub/processor"
	"gitlab.com/tromos/tromos-ce/engine/hub"
	"gitlab.com/tromos/tromos-ce/engine/manifest"
	"gitlab.com/tromos/tromos-ce/pkg/debug"
	"io"
	"io/ioutil"
	"sync"
	"testing"
)

var (
	Peer     = "127.0.0.1"
	Filesize = "1M"
	Manifest = "./processor_test.yml"

	MockingData []byte
	processors  []*Processor
	mani        *manifest.Manifest
)

func init() {
	viper.SetConfigFile(Manifest)
	if err := viper.ReadInConfig(); err != nil {
		panic(err)
	}

	man, err := manifest.NewManifest(viper.GetViper())
	if err != nil {
		panic(err)
	}
	mani = man

	// Find and download all the plugins for the peer
	pluginList := man.PluginsOnPeer(Peer)
	if len(pluginList) == 0 {
		panic("No plugins were defined")
	}

	// Download all the necessary plugins
	if err := hub.FixDependencies(pluginList); err != nil {
		panic(err)
	}

	filesize, err := bytefmt.ToBytes(Filesize)
	if err != nil {
		panic(err)
	}

	// Dummy data
	MockingData = make([]byte, filesize)

	// Set logger preferences
	logrus.SetLevel(logrus.DebugLevel)
}

func MockDeviceUpstream(wch processor.Channel) error {
	var wg sync.WaitGroup
	for stream := range wch.Readyports() {
		wg.Add(1)
		go func(stream processor.Stream) {
			defer wg.Done()

			reader := stream.Data.(processor.Receiver).PipeReader

			n, err := io.Copy(ioutil.Discard, reader)
			if err != nil {
				panic(err)
			}
			stream.Meta.SetItem(stream.Port, device.Item{
				ID:     "mocked",
				Size:   uint64(n),
				Offset: 0,
				Chain:  nil,
			})
		}(stream)
	}
	wg.Wait()
	return nil
}

func MockDeviceDownstream(rch processor.Channel) error {
	var wg sync.WaitGroup
	for stream := range rch.Readyports() {
		wg.Add(1)
		go func(stream processor.Stream) {
			defer wg.Done()

			pw := stream.Data.(processor.Sender).PipeWriter
			defer pw.Close()

			_, err := io.Copy(pw, bytes.NewReader(MockingData))
			if err != nil {
				panic(err)
			}

		}(stream)
	}
	wg.Wait()

	return nil
}

var streamLocker sync.Mutex

// A mocked transfer registers the intentions synchronously and perform the data copy asynchronously
// Like a future/promise model
func MockWrite(ch processor.Channel, streams *[]processor.Stream) error {
	pr, pw := processor.Pipe()

	stream := processor.Stream{
		Data: pr,
		Port: "mockwriter",
		Meta: &processor.StreamMetadata{
			//State: make(map[string]string),
			Items: make(map[string]device.Item),
		},
	}

	// Protect against race conditions on parallel writes
	streamLocker.Lock()
	*streams = append(*streams, stream)
	streamLocker.Unlock()

	if err := ch.NewTransfer(stream); err != nil {
		return err
	}

	go func() {
		defer pw.Close()
		_, err := io.Copy(pw, bytes.NewReader(MockingData))
		if err != nil {
			panic(err)
		}
	}()
	return nil
}

// A mocked transfer registers the intentions synchronously and perform the data copy asynchronously
// Like a future/promise model
func MockRead(ch processor.Channel, stream processor.Stream) error {
	pr, pw := processor.Pipe()

	newstream := processor.Stream{
		Data: pw,
		Port: "mockwriter",
		Meta: stream.Meta,
	}
	if err := ch.NewTransfer(newstream); err != nil {
		return err
	}

	go func() {
		_, err := io.Copy(ioutil.Discard, pr)
		if err != nil {
			panic(err)
		}
	}()
	return nil
}

func TestSynthesize(t *testing.T) {
	var proc *Processor
	var err error

	_, err = New(ProcessorConfig{})
	assert.Equal(t, err, processor.ErrInvalid, "Return error should be empty")

	// Register all the devices
	for _, procid := range mani.Processors() {
		logger := logrus.WithFields(logrus.Fields{
			"Processor": procid,
			"File":      debug.WhereAmI(),
		})

		config := ProcessorConfig{
			ID:         procid,
			Parameters: mani.ProcessorConfiguration(procid),
			Logger:     logger,
		}

		proc, err = New(config)

		assert.Nil(t, err)
		processors = append(processors, proc)
	}
}

func Test(t *testing.T) {
	for _, proc := range processors {
		// Fill the Channel with streams
		var streams []processor.Stream

		proc.config.Logger.Infof("Start sequential writing tests")
		// Pass the pointer as we may need to resize the slice as we append streams
		t.Run(proc.String(), testWrite(proc, &streams))
		t.Run(proc.String(), testWrite(proc, &streams))
		t.Run(proc.String(), testWrite(proc, &streams))

		proc.config.Logger.Infof("Start sequential reading tests")
		// No need for resizing here. The slide is fixed and known
		t.Run(proc.String(), testRead(proc, streams))
		t.Run(proc.String(), testRead(proc, streams))
		t.Run(proc.String(), testRead(proc, streams))
	}

	for _, proc := range processors {
		if err := proc.Close(); err != nil {
			panic(err)
		}
	}
}

func testWrite(proc *Processor, streams *[]processor.Stream) func(t *testing.T) {
	return func(t *testing.T) {
		sinks := make(map[string]string)

		// Open Channel
		wch, err := proc.NewChannel(
			processor.Logger(proc.config.Logger),
			processor.Writable(true),
			processor.Name("testing"),
			processor.Sinks(sinks),
		)
		assert.Nil(t, err)

		// Create Dumpers for the outgoing streams
		var wg sync.WaitGroup
		wg.Add(1)
		go func() {
			assert.Nil(t, MockDeviceUpstream(wch))
			wg.Done()
		}()

		for i := 0; i < 3; i++ {
			err := MockWrite(wch, streams)
			assert.Nil(t, err)
		}

		// Close channel
		assert.Nil(t, wch.Close())

		// Wait for dumpers. The equivalent is to wait for a Device's channel to terminate
		wg.Wait()
	}
}

func testRead(proc *Processor, streams []processor.Stream) func(t *testing.T) {
	return func(t *testing.T) {
		sinks := make(map[string]string)

		// Open Channel
		rch, err := proc.NewChannel(
			processor.Logger(proc.config.Logger),
			processor.Writable(false),
			processor.Name("testing"),
			processor.Sinks(sinks),
		)
		assert.Nil(t, err)

		// Create Dumpers for the outgoing streams
		var wg sync.WaitGroup
		wg.Add(1)
		go func() {
			assert.Nil(t, MockDeviceDownstream(rch))
			wg.Done()
		}()

		for _, stream := range streams {
			err := MockRead(rch, stream)
			assert.Nil(t, err)
		}

		// Close channel
		assert.Nil(t, rch.Close())

		// Wait for dumpers. The equivalent is to wait for a Device's channel to terminate
		wg.Wait()
	}
}
