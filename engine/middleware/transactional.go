// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package middleware // import "gitlab.com/tromos/tromos-ce/engine/middleware"

import (
	"encoding/json"
	"github.com/sirupsen/logrus"
	"gitlab.com/tromos/hub/processor"
	"gitlab.com/tromos/tromos-ce/pkg/assertion"
	"gitlab.com/tromos/tromos-ce/pkg/debug"
	"gitlab.com/tromos/tromos-ce/pkg/log"
	"gitlab.com/tromos/tromos-ce/pkg/uuid"
	"io"
	"os"
)

type Mode uint8

const (
	// Retrieve information, without distributed locks
	VIEW = Mode(iota)
	// Retrieve information, with distributed locks
	RDONLY
	// Write Only
	WONLY
)

// Return a string representation instead of a numerical
func (m Mode) String() string {
	return [...]string{"VIEW", "RDONLY", "WONLY"}[m]
}

func (client *Client) BeginTx(key string, mode Mode) (*Tx, error) {
	tid := uuid.Once()

	logger := logrus.WithFields(logrus.Fields{"TX": tid})
	logger.Data["file"] = log.FileInfo(2)
	logger.Level = logrus.DebugLevel
	logrus.SetOutput(os.Stdout)

	tx := &Tx{
		TID:    tid,
		key:    key,
		mode:   mode,
		client: client,
		logger: logger,
	}
	tx.logger.Infof("Initiate %s transaction for key %s", mode, key)

	partition := client.Namespace.Partition(key)

	switch mode {
	case VIEW:
		historyBins, _, err := partition.Info(key)
		if err != nil {
			return nil, err
		}

		cursor, err := populate(historyBins)
		if err != nil {
			return nil, err
		}
		tx.cursor = cursor

	case RDONLY:
		historyBins, _, err := partition.ViewStart(key, nil)
		if err != nil {
			return nil, err
		}

		cursor, err := populate(historyBins)
		if err != nil {
			return nil, err
		}
		tx.cursor = cursor

	case WONLY:

		uploader, err := client.Datapath.NewChannel(
			processor.Logger(tx.logger),
			processor.Writable(true),
			processor.Name(tx.TID),
			processor.Sinks(nil),
		)
		if err != nil {
			return nil, err
		}
		tx.uploader = uploader
		tx.Sinks = uploader.Sinks()

		ir, err := json.Marshal(tx.Sinks)
		if err != nil {
			return nil, err
		}

		if err := partition.UpdateStart(key, tx.ID(), ir); err != nil {
			return nil, err
		}
	}

	return tx, nil
}

type SubTx struct {
	Offset int `json:"Offset"`
	Size   int `json:"Size"`

	processor.Stream
}

type Tx struct {
	// Logger specific to the transaction
	logger *logrus.Entry

	// key in the keyspace
	key string

	// Operation mode
	mode Mode

	// Protected ensures that a transaction will not usable after its closing
	closed bool

	// Pointer to cursor
	cursor *Cursor

	// Pointer to the middleware
	client *Client

	// UpdateTx
	SubTxs []*SubTx `json:"SubTxs,omitempty"`

	Sinks map[string]string `json:"Sinks,omitempty"`

	// transaction identifier
	TID string `json:"TID,omitempty"`

	// The uploader for this transaction
	uploader processor.Channel

	// Stream Identifier
	nextStream int
}

// ID returns the transaction id
func (tx *Tx) ID() string {
	return tx.TID
}

func (tx *Tx) Key() string {
	return tx.key
}

func (tx *Tx) Update(offset int64, fn func(pw io.ReadWriteCloser) (int, error)) (int, error) {
	log.Trace("-> ", debug.WhereAmI())
	defer log.Trace("<- ", debug.WhereAmI())

	assertion.Assert(tx.mode == WONLY, "non writable transaction")
	assertion.Assert(!tx.closed, "transaction is closed")

	pr, pw := processor.Pipe()

	stream := processor.Stream{
		Data: pr,
		Port: "Root",
		Meta: &processor.StreamMetadata{ID: tx.nextStream},
	}
	tx.nextStream++

	if err := tx.uploader.NewTransfer(stream); err != nil {
		return 0, err
	}

	stx := &SubTx{
		Stream: stream,
		Offset: int(offset),
	}

	tx.SubTxs = append(tx.SubTxs, stx)

	wb, err := fn(pw)
	if err != nil {
		if err := tx.Rollback(); err != nil {
			panic(err)
		}
		return wb, err
	}

	if err := pw.Close(); err != nil {
		if err := tx.Rollback(); err != nil {
			panic(err)
		}
		return wb, err
	}

	stx.Size = wb

	return wb, nil
}

// We want the reader to be stateless, but doing so when blocks are involved
// is tricky. Offset M may not be aligned with block begin at offset N.
// In order to navigate correctly the metadata within the block the client
// must position with M-N (alpabetically ordered)
func (tx *Tx) Get(segment uint64) (int, []byte, error) {
	log.Trace("-> ", debug.WhereAmI())
	defer log.Trace("<- ", debug.WhereAmI())

	assertion.Assert(tx.mode == VIEW || tx.mode == RDONLY, "non viewable transaction")

	if tx.closed {
		panic("Reader is closed")
	}

	utx := tx.cursor.stx2Tx[segment]
	stx := tx.cursor.stxs[segment]
	data := make([]byte, stx.Size)

	pr, pw := processor.Pipe()

	// Fix the downloader to the downloader list
	downloader, ok := tx.cursor.downloaders[utx.ID()]
	if !ok {
		ch, err := tx.client.Datapath.NewChannel(
			processor.Logger(tx.logger),
			processor.Writable(false),
			processor.Name(tx.TID),
			processor.Sinks(utx.Sinks),
		)
		if err != nil {
			return 0, nil, err
		}
		tx.cursor.downloaders[utx.ID()] = ch
		downloader = ch
	}

	stream := processor.Stream{
		Data: pw,
		Port: "root",
		Meta: &processor.StreamMetadata{
			ID:    tx.nextStream,
			State: stx.Meta.State,
			Items: stx.Meta.Items,
		},
	}
	tx.nextStream++

	if err := downloader.NewTransfer(stream); err != nil {
		return 0, nil, err
	}

	rb, err := io.ReadFull(pr, data)
	if err != nil {
		return stx.Offset, data[:rb], err
	}
	return stx.Offset, data[:rb], nil
}

// Commit writes all changes to the devices and updates the meta page on coordinators
// Returns an error if a device write error occurs, or if Commit is called on a read-only transaction.
func (tx *Tx) Commit() error {
	assertion.Assert(tx.mode == WONLY, "Commit is called on read-only transaction")
	assertion.Assert(!tx.closed, "transaction is closed")

	// Synchronously terminate iopath
	if err := tx.uploader.Close(); err != nil {
		return err
	}

	var ur []byte
	ur, err := json.Marshal(tx)
	if err != nil {
		return err
	}

	partition := tx.client.Namespace.Partition(tx.key)
	if err := partition.UpdateEnd(tx.key, tx.TID, ur); err != nil {
		return err
	}

	tx.logger.Infof("Transaction successfully commited to %s", partition.String())
	return nil
}

// Rollback closes the transaction and ignores all previous updates.
// Read-only transactions must be rolled back and not committed.
func (tx *Tx) Rollback() error {
	switch tx.mode {

	case VIEW:
		// Release all locked resources
		for _, d := range tx.cursor.downloaders {
			if err := d.Close(); err != nil {
				panic(err)
			}
		}

	case RDONLY:
		// Release all locked resources
		for _, d := range tx.cursor.downloaders {
			if err := d.Close(); err != nil {
				panic(err)
			}
		}

		// In this type of transaction the Coordinators keep a lease
		// on entries so to avoid being garbage collected. For the rollback,
		// we must notify the Cooordinator to release the lease
		partition := tx.client.Namespace.Partition(tx.Key())
		if err := partition.ViewEnd(tx.cursor.allocated); err != nil {
			return err
		}

	case WONLY:
		// TODO
		panic("Rollback not supported yet for writable transactions")

	}

	tx.logger.Infof("Transaction successfully rolledback")
	return nil
}

func (tx *Tx) Cursor() *Cursor {
	assertion.Assert(tx.mode != WONLY, "Commit is called on write-only transaction")
	assertion.Assert(!tx.closed, "transaction is closed")

	return tx.cursor
}

type Cursor struct {
	// List of backend metadata (for all handlers)
	stxs []*SubTx

	stx2Tx []*Tx

	// List of locked updates (not eligible for garbage collection)
	allocated []string

	// map[TID]downloader
	downloaders map[string]processor.Channel
}

// ForEach executes a function for each delta in the transaction.
// If false, stop iteration
func (c *Cursor) ForEach(fn func(stx *SubTx) bool) {
	for _, stx := range c.stxs {
		if ok := fn(stx); !ok {
			return
		}
	}
}

func populate(historyBins [][]byte) (*Cursor, error) {

	if len(historyBins) == 0 {
		panic("It is not allowed to give empty history bins")
	}

	c := &Cursor{
		downloaders: make(map[string]processor.Channel),
	}

	updates := make([]*Tx, len(historyBins))
	for i, bin := range historyBins {
		if err := json.Unmarshal(bin, &updates[i]); err != nil {
			return nil, err
		}

		for j := 0; j < len(updates[i].SubTxs); j++ {
			stx := updates[i].SubTxs[j]

			if stx.Size > 0 {
				c.stxs = append(c.stxs, stx)
				c.stx2Tx = append(c.stx2Tx, updates[i])
			}
		}

		c.allocated = append(c.allocated, updates[i].ID())
	}
	return c, nil
}
