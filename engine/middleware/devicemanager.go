// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package middleware // import "gitlab.com/tromos/tromos-ce/engine/middleware"

import (
	"gitlab.com/tromos/hub/device"
	"gitlab.com/tromos/hub/selector"
	//rpc "github.com/hprose/hprose-golang/rpc/websocket"
	"gitlab.com/tromos/tromos-ce/engine/peer"
	//"gitlab.com/tromos/tromos-ce/pkg/log"
	"gopkg.in/go-playground/validator.v9"
	"gitlab.com/tromos/tromos-ce/pkg/net"
	//"net/http"
	"sync"
)

type DeviceManagerConfig struct {
	//	WebService string            `validate:"isdefault|ip"`
	Peer     *peer.Peer        `validate:"required"`
	Selector selector.Selector `validate:"required"`
}

// DeviceManager embeds cluster knowledge for all the device in the container.
// It is a distributed service running on every node that hosts or wants to have access
// to Device. it is assumed that all distributed instances share the cluster knowledge.
//
// When a client wants to access a Device, it asks the Device Manager to return
// the appropriate connector. However, the insitu Device manager may differ from the
// authority Device Manager (the one responsible for the selection and reservations). This is
// the case for datapaths whose leaves are on different nodes than the client. In that
// case, the insitu Device Manager forwards the request to the Authority Device manager
type DeviceManager struct {
	locker       sync.Mutex
	config       *DeviceManagerConfig
	mesh         map[string]device.Device
	reservations map[string][]string // request:exludeList

	//webservice *http.Server
	//authorities map[string]*authorityClient
}

func NewDeviceManager(config *DeviceManagerConfig) (*DeviceManager, error) {
	// validate the configuration
	validate := validator.New()
	if err := validate.Struct(config); err != nil {
		return nil, err
	}

	dm := &DeviceManager{
		config:       config,
		mesh:         make(map[string]device.Device),
		reservations: make(map[string][]string),
		//authorities:  make(map[string]*authorityClient),
	}

	for devid, dev := range config.Peer.Devices() {
		dm.mesh[devid] = dev
		dm.config.Selector.Add(selector.Properties{
			ID:           devid,
			Capabilities: dev.Capabilities(),
			Peer:         dev.Location(),
		})
	}
	dm.config.Selector.Commit()
	/*
		// Optionally make the query functions of the device manager accessible through the network
		// This is need when the Authority Device Manager is other than the insitu Device Manager
		// (the case for datapaths that span across several nodes)
		if dm.config.WebService == "" {
			ops := rpc.NewWebSocketService()
			ops.AddFunction("ProxySelectAndReserve", dm.SelectAndReserve)
			webservice := &http.Server{
				Addr:    dm.config.WebService,
				Handler: ops,
			}
			dm.webservice = webservice

			go func() {
				if err := webservice.ListenAndServe(); err != nil {
					panic(err)
				}
			}()
		}
	*/
	return dm, nil
}

// Close closes the device manager
func (dm *DeviceManager) Close() error {
	return nil
	//	return dm.webservice.Shutdown(context.TODO())
}

// Reserve asks the Authority Device manager to select and reserve
// one of the available Devices in the cluster. It returns a connector to the selected Device
func (dm *DeviceManager) Reserve(authority string, tid string, capabilities ...selector.Capability) (device.Device, error) {

	var deviceID string
	var err error

	// If the indicatd webservice is running locally, do not go through the network
	if net.IsLocalAddress(authority) {
		deviceID, err = dm.SelectAndReserve(tid, capabilities...)
		if err != nil {
			return nil, err
		}
	} /*
		else {
			client := dm.getAuthorityClient(authority)
			deviceID, err = client.call.SelectAndReserve(tid, capabilities...)
			if err != nil {
				return nil, err
			}
		}
	*/
	return dm.GetDevice(deviceID)
}

/*
type authorityClient struct {
	conn *rpc.WebSocketClient
	call *WebServiceOperations
}

func (dm *DeviceManager) getAuthorityClient(service string) *authorityClient {

	// reuse client from a pool, if the connection to authority device manager
	// already exists
	cli, ok := dm.authorities[service]
	if ok {
		return cli
	}

	call := &WebServiceOperations{}
	conn := rpc.NewWebSocketClient("ws://" + service + "/")
	//client.SetTimeout(cli.timeout)
	conn.UseService(call)

	return &authorityClient{call: call, conn: conn}
}
*/
// SelectAndReserve selects and reserves one of the available Devices in the cluster based
// on the capability contrains. It returns the Device ID
func (dm *DeviceManager) SelectAndReserve(tid string, capabilities ...selector.Capability) (string, error) {

	dm.locker.Lock()
	defer dm.locker.Unlock()

	reserved := dm.reservations[tid]

	selected, err := dm.config.Selector.Select(reserved, capabilities...)
	if err != nil {
		return "", err
	}

	// Exclude from the selection any device that is already selected
	// for the current transaction
	reserved = append(reserved, selected)
	dm.reservations[tid] = reserved

	return selected, nil
}

// GetDevice returns a connector to the Device identified by deviceID
func (dm *DeviceManager) GetDevice(deviceID string) (device.Device, error) {

	if deviceID == "" {
		panic("DeviceID should not be empty")
	}
	d, ok := dm.mesh[deviceID]
	if !ok {
		return nil, ErrStaleCluster
	}
	return d, nil
}
