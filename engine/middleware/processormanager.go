// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package middleware // import "gitlab.com/tromos/tromos-ce/engine/middleware"

import (
	"gitlab.com/tromos/hub/processor"
	"gitlab.com/tromos/hub/selector"
	"gopkg.in/go-playground/validator.v9"
	"gitlab.com/tromos/tromos-ce/engine/peer"
)

type ProcessorManagerConfig struct {
	Peer     *peer.Peer        `validate:"required"`
	Selector selector.Selector `validate:"required"`
}

// ProcessorManager embeds cluster knowledge for all the Processor in the container.
// It is a distributed service running on every node that hosts or wants to have access
// to Processor. it is assumed that all distributed instances share the cluster knowledge.
//
// When a client wants to access a Processor, it asks the Processor Manager to return
// the appropriate connector.
type ProcessorManager struct {
	config *ProcessorManagerConfig
	mesh   map[string]processor.Processor
}

func NewProcessorManager(config *ProcessorManagerConfig) (*ProcessorManager, error) {

	// validate the configuration
	validate := validator.New()
	if err := validate.Struct(config); err != nil {
		return nil, err
	}

	pm := &ProcessorManager{
		config: config,
		mesh:   make(map[string]processor.Processor),
	}

	for pid, proc := range config.Peer.Processors() {
		pm.mesh[pid] = proc
		pm.config.Selector.Add(selector.Properties{
			ID:           pid,
			Capabilities: proc.Capabilities(),
			Peer:         proc.Location(),
		})
	}
	pm.config.Selector.Commit()

	return pm, nil
}

// Close closes the device manager
func (pm *ProcessorManager) Close() error {
	return nil
}

// SelectAndReserve selects and reserves one of the available Processors in the cluster based
// on the capability contrains. It returns the Processor ID
func (pm *ProcessorManager) SelectAndReserve(capabilities ...selector.Capability) (processor.Processor, error) {
	procid, err := pm.config.Selector.Select(nil, capabilities...)
	if err != nil {
		return nil, err
	}
	return pm.GetProcessor(procid)
}

// GetProcessor returns a connector to the Processor identified by processorID
func (pm *ProcessorManager) GetProcessor(processorID string) (processor.Processor, error) {
	proc, ok := pm.mesh[processorID]
	if !ok {
		return nil, ErrStaleCluster
	}
	return proc, nil
}
