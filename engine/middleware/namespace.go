// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package middleware // import "gitlab.com/tromos/tromos-ce/engine/middleware"

import (
	"gitlab.com/tromos/hub/coordinator"
	"gitlab.com/tromos/hub/selector"
	"gitlab.com/tromos/tromos-ce/engine/peer"
	"gitlab.com/tromos/tromos-ce/pkg/debug"
	"gitlab.com/tromos/tromos-ce/pkg/log"
	"gopkg.in/go-playground/validator.v9"
)

type NamespaceConfig struct {
	Peer     *peer.Peer        `validate:"required"`
	Selector selector.Selector `validate:"required"`
}

type Namespace struct {
	config *NamespaceConfig
	mesh   map[string]coordinator.Coordinator
}

func NewNamespace(config *NamespaceConfig) (*Namespace, error) {

	// validate the configuration
	validate := validator.New()
	if err := validate.Struct(config); err != nil {
		return nil, err
	}

	nm := &Namespace{
		config: config,
		mesh:   make(map[string]coordinator.Coordinator),
	}

	for cid, coord := range config.Peer.Coordinators() {
		nm.mesh[cid] = coord
		nm.config.Selector.Add(selector.Properties{
			ID:           cid,
			Capabilities: coord.Capabilities(),
			Peer:         coord.Location(),
		})
	}
	nm.config.Selector.Commit()

	return nm, nil
}

// Close shutdowns all the coordinators of the namespace
func (nm *Namespace) Close() error {
	log.Trace("-> ", debug.WhereAmI())
	defer log.Trace("<- ", debug.WhereAmI())

	nm.config.Selector.Walk(func(k []byte, _ interface{}) bool {
		if err := nm.mesh[string(k)].Close(); err != nil {
			return true
		}
		return false
	})
	return nil
}

// Partition returns the Coordinator resposinble for the key
func (nm *Namespace) Partition(key string) coordinator.Coordinator {
	log.Trace("-> ", debug.WhereAmI())
	defer log.Trace("<- ", debug.WhereAmI())

	proposed, err := nm.config.Selector.Partition(key)
	if err != nil {
		panic(err)
	}
	return nm.mesh[proposed]
}
