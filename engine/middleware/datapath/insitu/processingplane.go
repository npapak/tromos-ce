// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package datapath // import "gitlab.com/tromos/tromos-ce/engine/middleware/datapath/insitu"

import (
	"github.com/trustmaster/goflow"
	"gitlab.com/tromos/hub/processor"
	"gitlab.com/tromos/tromos-ce/engine/middleware/datapath"
)

type ProcessPlane struct {
	flow.Component

	In  <-chan *datapath.Discovery
	Out chan<- *datapath.Discovery

	processor.Processor
}

func (plane *ProcessPlane) OnIn(req *datapath.Discovery) {
	newReservation(req, plane.Processor, plane.Out)
}

func newReservation(req *datapath.Discovery, processor processor.Processor, outs chan<- *datapath.Discovery) {

	ch, err := processor.NewChannelFromConfig(req.ChannelConfig)
	if err != nil {
		datapath.Abort(req, err)
		return
	}

	// discover and reserve links with the components connected to the Processor's outputs
	nextHops := make(map[string]*datapath.Reservation)
	for port := range ch.Sinks() {

		reservation, err := datapath.Discover(outs, req, port, ch.Capabilities(port)...)
		if err != nil {
			datapath.Abort(req, err)
			return
		}
		nextHops[port] = reservation
	}
	reservation := datapath.Reserve(req)
	reservation.Logger().Debug("Processor Reserved.  Start listening")

	for in := range reservation.Listen() {
		reservation.Logger().Debugf("Processor serving Stream %d", in.Meta.ID)

		if err := ch.NewTransfer(in); err != nil {
			datapath.Abort(req, err)
			return
		}

		// Write the outputs of the stream (process by the graph) to the next hops.
		// A trick is needed here for termination after all the ports of the graph are writte.
		// This is because the Readyports is unbounded and loops waiting for a signal to come
		completed := 0
		for substream := range ch.Readyports() {
			nextHops[substream.Port].NewTransfer(substream)
			completed++

			if completed == len(nextHops) {
				break
			}
		}
	}

	//	Terminate processor
	for _, nexthop := range nextHops {
		nexthop.WaitRelease()
	}

	reservation.Release()
	reservation.Logger().Debug("Processor Reservation has terminated")
}
