// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package datapath // import "gitlab.com/tromos/tromos-ce/engine/middleware/datapath/insitu"

import (
	"github.com/trustmaster/goflow"
	"gitlab.com/tromos/hub/processor"
	"gitlab.com/tromos/hub/selector"
	"gitlab.com/tromos/tromos-ce/engine/middleware/datapath"
	"gopkg.in/go-playground/validator.v9"
)

// LocalDatapath connect insitu processor with insitu device manager
type LocalDatapath struct {
	config   datapath.Config
	discover chan *datapath.Discovery
	network  *flow.Graph
}

func NewLocalDatapath(opts ...datapath.Option) (*LocalDatapath, error) {

	config := datapath.Config{}
	for _, opt := range opts {
		opt(&config)
	}

	// validate the configuration
	validate := validator.New()
	if err := validate.Struct(config); err != nil {
		return nil, err
	}

	proc, err := config.ProcessorManager.SelectAndReserve()
	if err != nil {
		return nil, err
	}

	pplane := &ProcessPlane{Processor: proc}
	splane := &StoragePlane{}

	// Init IOdiscover network (the custom graph that will be used)
	network := new(flow.Graph)
	network.InitGraphState()

	network.Add(pplane, "InTransit")
	network.Add(splane, "Storage")
	network.MapInPort("In", "InTransit", "In")
	network.Connect("InTransit", "Out", "Storage", "In")

	discover := make(chan *datapath.Discovery)
	network.SetInPort("In", discover)

	flow.RunNet(network)
	<-network.Ready()

	return &LocalDatapath{
		config:   config,
		discover: discover,
		network:  network,
	}, nil
}

func (_ *LocalDatapath) String() string {
	return "LocalDatapath"
}

func (_ *LocalDatapath) Reusable() bool {
	return false
}

func (_ *LocalDatapath) Capabilities() []selector.Capability {
	return nil
}

func (_ *LocalDatapath) Location() string {
	return ""
}

func (path *LocalDatapath) Close() error {
	// FIXME: Should I use it ?
	close(path.discover)
	<-path.network.Wait()
	return nil
}

func (path *LocalDatapath) NewChannel(opts ...processor.ChannelOption) (processor.Channel, error) {

	var config processor.ChannelConfig
	for _, opt := range opts {
		opt(&config)
	}
	return path.NewChannelFromConfig(config)
}

func (path *LocalDatapath) NewChannelFromConfig(config processor.ChannelConfig) (processor.Channel, error) {

	// TODO: if ever migrate to distributedDatapath, do not forget to set the
	// Authority Device Manager to the insitu device manager of the client
	// who initiates the channel (or to the central Authority Device Manager, if any)
	// To do so, use the cluster-facing ip into the WalkID
	request := &datapath.Discovery{
		ChannelConfig: config,
		DeviceManager: path.config.DeviceManager,
	}

	reservation, err := datapath.Discover(path.discover, request, "127.0.0.1")
	if err != nil {
		return nil, err
	}

	return &channel{
		request:  request,
		response: reservation,
	}, nil

}

type channel struct {
	request  *datapath.Discovery
	response *datapath.Reservation
}

func (ch *channel) NewTransfer(stream processor.Stream) error {
	ch.response.NewTransfer(stream)
	return nil
}

func (ch *channel) Close() error {
	ch.response.WaitRelease()
	return nil
}

func (ch *channel) Capabilities(port string) []selector.Capability {
	panic("Should not be called")
}

func (ch *channel) Sinks() map[string]string {
	return ch.request.ChannelConfig.Sinks
}

func (ch *channel) Readyports() <-chan processor.Stream {
	panic("Should not be called")
}
