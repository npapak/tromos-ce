// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package datapath // import "gitlab.com/tromos/tromos-ce/engine/middleware/datapath/insitu"

import (
	"github.com/sirupsen/logrus"
	"github.com/trustmaster/goflow"
	"gitlab.com/tromos/hub/device"
	"gitlab.com/tromos/hub/processor"
	"gitlab.com/tromos/tromos-ce/engine/middleware/datapath"
)

func init() {
	logrus.WithField("module", "datapath")
}

type transfer struct {
	in  processor.Stream
	out *device.Stream
}

type StoragePlane struct {
	flow.Component
	logger *logrus.Entry

	In  <-chan *datapath.Discovery
	Out chan<- *datapath.Discovery
}

func (plane *StoragePlane) OnIn(req *datapath.Discovery) {
	if req.ChannelConfig.Writable {
		Upstream(req)
	} else {
		Downstream(req)
	}
}

func Upstream(req *datapath.Discovery) {
	// datapath - Create channels with devices
	dev, err := req.DeviceManager.Reserve(
		datapath.Authority(req),
		req.ChannelConfig.Name,
		req.Capabilities...,
	)
	if err != nil {
		datapath.Abort(req, err)
		return
	}

	channel, err := dev.NewWriteChannel(req.ChannelConfig.Name)
	if err != nil {
		datapath.Abort(req, err)
		return
	}
	req.ConfigLocker.Lock()
	req.ChannelConfig.Sinks[datapath.WalkID(req)] = dev.String()
	req.ConfigLocker.Unlock()

	reservation := datapath.Reserve(req)
	reservation.Logger().Debug("Storage Reserved.  Start listening")

	// And run (I/O Streams)
	var transfers []*transfer
	for in := range reservation.Listen() {
		transfer := &transfer{
			in:  in,
			out: &device.Stream{Complete: make(chan struct{})},
		}

		reader := transfer.in.Data.(processor.Receiver).PipeReader
		if err := channel.NewTransfer(reader, transfer.out); err != nil {
			panic(err)
		}
		transfers = append(transfers, transfer)
	}

	if err := channel.Close(); err != nil {
		datapath.Abort(req, err)
		return
	}

	for _, transfer := range transfers {
		transfer.in.Meta.SetItem(datapath.WalkID(req), transfer.out.Item)

		reservation.Logger().Debugf("Stream [%s] -> [%s] wrote %d bytes",
			datapath.WalkID(req), dev.String(), transfer.out.Item.Length())
	}

	reservation.Release()
	reservation.Logger().Debug("Storage Reservation has terminated")
}

func Downstream(req *datapath.Discovery) {
	deviceID := req.ChannelConfig.Sinks[datapath.WalkID(req)]

	dev, err := req.DeviceManager.GetDevice(deviceID)
	if err != nil {
		datapath.Abort(req, err)
		return
	}

	channel, err := dev.NewReadChannel(req.ChannelConfig.Name)
	if err != nil {
		datapath.Abort(req, err)
		return
	}

	reservation := datapath.Reserve(req)
	reservation.Logger().Debug("Storage Reserved. Start listening")

	for parent := range reservation.Listen() {
		reservation.Logger().Debug("Storage NewStream")
		// It is possible that data are contained only in a subset of the devices.
		// This can be either due to scan where data are located only
		// on a specific device and the rest are nil, or like strip
		// where dataholders have been created but not populated with data
		item, ok := parent.Meta.GetItem(datapath.WalkID(req))
		if !ok || item.IsEmpty() {
			// Terminate the writer. No need to go further into the storage line
			if err := parent.Data.Close(); err != nil {
				panic(err)
			}

		} else {
			writer := parent.Data.(processor.Sender).PipeWriter
			err := channel.NewTransfer(writer, &device.Stream{Item: item})
			if err != nil {
				if err := writer.CloseWithError(err); err != nil {
					panic(err)
				}
			}
		}
	}

	if err := channel.Close(); err != nil {
		datapath.Abort(req, err)
		return
	}

	reservation.Release()
	reservation.Logger().Debug("Storage Reservation has terminated")
}
