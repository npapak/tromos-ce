// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package datapath // import "gitlab.com/tromos/tromos-ce/engine/middleware/datapath"

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/tromos/hub/device"
	"gitlab.com/tromos/hub/processor"
	"gitlab.com/tromos/hub/selector"
	"strings"
	"sync"
)

type ProcessorManager interface {
	Close() error
	SelectAndReserve(capabilities ...selector.Capability) (processor.Processor, error)
	GetProcessor(processorID string) (processor.Processor, error)
}

type DeviceManager interface {
	Close() error
	Reserve(authority string, tid string, capabilities ...selector.Capability) (device.Device, error)
	SelectAndReserve(tid string, capabilities ...selector.Capability) (string, error)
	GetDevice(deviceID string) (device.Device, error)
}

type Config struct {
	ProcessorManager ProcessorManager `validate:"required"`
	DeviceManager    DeviceManager    `validate:"required"`
}

type Option func(*Config)

// Processor defines the insitu processor for the node
func WithProcessorManager(pm ProcessorManager) Option {
	return func(c *Config) {
		c.ProcessorManager = pm
	}
}

// DeviceManager defines the insitu device manager for the node
func WithDeviceManager(dm DeviceManager) Option {
	return func(c *Config) {
		c.DeviceManager = dm
	}
}

func WalkID(req *Discovery) string {
	return strings.Join(req.walkID, ".")
}

func Authority(req *Discovery) string {
	return req.walkID[0]
}

// The first element of the WalkID is the authority Device Manager
type Discovery struct {
	ConfigLocker  sync.Mutex
	ChannelConfig processor.ChannelConfig `validate:"required"`

	DeviceManager DeviceManager `validate:"required"`

	Capabilities []selector.Capability
	waitresponse chan interface{}
	walkID       []string
}

func Discover(nexthop chan<- *Discovery, current *Discovery, port string, capabilities ...selector.Capability) (*Reservation, error) {

	waitresponse := make(chan interface{})

	// Create links with the elements on the outputs
	// Remember to expand the capabilities, otherwise it will be placed as
	// []interface{} and will screw the capability parsing
	nexthop <- &Discovery{
		ChannelConfig: current.ChannelConfig,
		DeviceManager: current.DeviceManager,
		Capabilities:  append(current.Capabilities, capabilities...),
		walkID:        append(current.walkID, port),
		waitresponse:  waitresponse,
	}

	resp := <-waitresponse
	switch resp.(type) {
	case error:
		return nil, resp.(error)
	case *Reservation:
		return resp.(*Reservation), nil
	default:
		panic("Unknown datapath response type")
	}
}

func Reserve(req *Discovery) *Reservation {
	reservation := &Reservation{
		request:  req,
		listener: make(chan processor.Stream, viper.GetInt("MaxConcurrentStreamsPerChannel")),
		alive:    make(chan struct{}),
	}
	req.waitresponse <- reservation
	close(req.waitresponse)
	return reservation
}

func Abort(req *Discovery, err error) {
	req.ChannelConfig.Logger.Errorf("Error:", err)
	req.waitresponse <- err
	close(req.waitresponse)
}

type Reservation struct {
	request  *Discovery
	listener chan processor.Stream // Closing listener : top-bottom
	alive    chan struct{}         // Closing report: bottom-up
}

// NewTransfer initiates a new asynchronous transfer
func (res *Reservation) NewTransfer(stream processor.Stream) {
	res.listener <- stream
}

// Listen return blocks waiting for incoming streams
func (res *Reservation) Listen() chan processor.Stream {
	return res.listener
}

// Release discards the reservation
func (res *Reservation) Release() {
	close(res.alive)
}

// Sinks returns the output of the present reservation branch
func (res *Reservation) Sinks() map[string]string {
	return res.request.ChannelConfig.Sinks
}

// WaitRelease blocks until the reservation is released
func (res *Reservation) WaitRelease() {
	close(res.listener)
	<-res.alive
}

func (res *Reservation) Logger() *logrus.Entry {
	return res.request.ChannelConfig.Logger
}
