// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package manifest // import "gitlab.com/tromos/tromos-ce/engine/manifest"

import (
	"bytes"
	"github.com/spf13/viper"
	"gitlab.com/tromos/tromos-ce/pkg/structures"
)

type Manifest struct {
	source *viper.Viper
}

func NewManifest(source *viper.Viper) (*Manifest, error) {
	return &Manifest{
		source: source,
	}, nil
}

// PluginsOnPeer returns all the plugin dependencies for the node with
// the given peer (we are interested only on the ip).
// TODO: Add support for wildcards
func (m *Manifest) PluginsOnPeer(peer string) []string {
	var pluginlist []string

	dm, _ := m.DeviceManagerConfiguration()
	pluginlist = append(pluginlist, dm)

	pm, _ := m.ProcessorManagerConfiguration()
	pluginlist = append(pluginlist, pm)

	namespace, _ := m.NamespaceConfiguration()
	pluginlist = append(pluginlist, namespace)

	for _, dev := range m.Devices() {
		pluginlist = append(pluginlist, m.DeviceStack(peer, dev)...)
	}

	for _, coord := range m.Coordinators() {
		pluginlist = append(pluginlist, m.CoordinatorStack(peer, coord)...)
	}

	for _, proc := range m.Processors() {
		pluginlist = append(pluginlist, m.ProcessorStack(peer, proc)...)
	}

	return structures.UniqueStrings(pluginlist)
}

// ID returns the manifest's identifier
func (m *Manifest) ID() string {
	return m.source.GetString("identifier")
}

// Description returns the manifest's description
func (m *Manifest) Description() string {
	return m.source.GetString("description")
}

// Devices return all the Devices in the Manifest
func (m *Manifest) Devices() []string {
	var devices []string
	devmap := m.source.GetStringMap("devices")
	for dev := range devmap {
		devices = append(devices, dev)
	}
	return devices
}

// DeviceStack returns the Device as presented on the node where the functions is running
// For example, is the device is insitu it will present the whole stack. Otherwise, it will
// show only the proxy client. The returned values are normalized (e.g., device/filesystem)
func (m *Manifest) DeviceStack(peer string, devid string) (stack []string) {

	// TODO: get the sub configuration from DeviceConfiguration instead of accessing
	// the whole path

	persistent := "devices." + devid + ".persistent.plugin"
	stack = append(stack, m.source.GetString(persistent))

	translators := m.source.GetStringMap("devices." + devid + ".translators")
	for _, seqID := range structures.SortMapStringKeys(translators) {
		translator := "devices." + devid + ".translators." + seqID + ".plugin"
		stack = append(stack, m.source.GetString(translator))
	}

	proxy := "devices." + devid + ".proxy"
	if !m.source.IsSet(proxy) {
		return stack
	}

	var islocal = false
	if peer == m.source.GetString(proxy+".host") {
		islocal = true
	}

	if islocal {
		stack = append(stack, m.source.GetString(proxy+".plugin")+"/server")
		stack = append(stack, m.source.GetString(proxy+".plugin")+"/client")
	} else {
		return []string{m.source.GetString(proxy+".plugin") + "/client"}
	}
	return stack
}

// DeviceConfiguration returns the whole configuration for a device
func (m *Manifest) DeviceConfiguration(devid string) *viper.Viper {
	return m.source.Sub("devices." + devid)
}

// Coordinators returns all the Coordinators in the Manifest
func (m *Manifest) Coordinators() []string {
	var coordinators []string
	coordmap := m.source.GetStringMap("coordinators")
	for coord := range coordmap {
		coordinators = append(coordinators, coord)
	}
	return coordinators
}

// CoordinatorStack returns the Coordinator as presented on the node where the functions is running
// For example, is the Coordinator is insitu it will present the whole stack. Otherwise, it will
// show only the proxy client. The returned values are normalized (e.g., coordinator/boltdb)
func (m *Manifest) CoordinatorStack(peer string, cid string) (stack []string) {

	// TODO: get the sub configuration from CoordinatorConfiguration instead of accessing
	// the whole path

	persistent := "coordinators." + cid + ".persistent.plugin"
	stack = append(stack, m.source.GetString(persistent))

	translators := m.source.GetStringMap("coordinators." + cid + ".translators")
	for _, seqID := range structures.SortMapStringKeys(translators) {
		translator := "coordinators." + cid + ".translators." + seqID + ".plugin"
		stack = append(stack, m.source.GetString(translator))
	}

	proxy := "coordinators." + cid + ".proxy"
	if !m.source.IsSet(proxy) {
		return stack
	}

	var islocal = false
	if peer == m.source.GetString(proxy+".host") {
		islocal = true
	}

	if islocal {
		stack = append(stack, m.source.GetString(proxy+".plugin")+"/server")
		stack = append(stack, m.source.GetString(proxy+".plugin")+"/client")
	} else {
		return []string{m.source.GetString(proxy+".plugin") + "/client"}
	}
	return stack
}

// CoordinatorConfiguration returns the whole configuration for a Coordinator
func (m *Manifest) CoordinatorConfiguration(cid string) *viper.Viper {
	return m.source.Sub("coordinators." + cid)
}

// Processors returns all the processors in the Manifest. If there are not processors, it
// returns the default Processor
func (m *Manifest) Processors() []string {
	var processors []string
	procmap := m.source.GetStringMap("processors")
	if len(procmap) == 0 {
		return []string{"defaultProcessor"}
	}

	for proc := range procmap {
		processors = append(processors, proc)
	}
	return processors
}

// ProcessorStack returns the Processor as presented on the node where the functions is running
// For example, is the Coordinator is insitu it will present the whole stack. Otherwise, it will
// show only the proxy client. The returned values are normalized (e.g., processor/rain)
func (m *Manifest) ProcessorStack(peer string, pid string) (stack []string) {
	conf := m.ProcessorConfiguration(pid)
	return []string{conf.GetString("graph.plugin")}
}

// ProcessorConfiguration returns the whole configuration for a Processor.
func (m *Manifest) ProcessorConfiguration(pid string) *viper.Viper {
	if pid == "defaultProcessor" {
		defaultConfig := viper.New()
		defaultConfig.SetConfigType("yaml")
		err := defaultConfig.ReadConfig(bytes.NewBuffer(DefaultProcessor))
		if err != nil {
			panic(err)
		}
		return defaultConfig
	}

	return m.source.Sub("processors." + pid)
}

// NamespaceConfiguration returns the plugin for the Coordinator selector and its configuration
func (m *Manifest) NamespaceConfiguration() (string, *viper.Viper) {
	sel := m.source.Sub("middleware.namespace")
	if sel == nil {
		return DefaultNamespace, nil
	}
	return sel.GetString("plugin"), sel
}

// DeviceManagerConfiguration returns the plugin for the Device selector and its configuration
func (m *Manifest) DeviceManagerConfiguration() (string, *viper.Viper) {
	sel := m.source.Sub("middleware.devicemanager")
	if sel == nil {
		return DefaultDeviceManager, nil
	}
	return sel.GetString("plugin"), sel
}

// ProcessorManagerConfiguration returns the plugin for the Processor selector and its configuration.
// there is no ProcessorManager defined in the Manifest, it will return the default ProcessorManager
func (m *Manifest) ProcessorManagerConfiguration() (string, *viper.Viper) {
	sel := m.source.Sub("middleware.processormanager")
	if sel == nil {
		sel = viper.New()
		sel.SetConfigType("yaml")
		err := sel.ReadConfig(bytes.NewBuffer(DefaultProcessorManager))
		if err != nil {
			panic(err)
		}
	}
	return sel.GetString("plugin"), sel
}
