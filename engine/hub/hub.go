// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package hub // import "gitlab.com/tromos/tromos-ce/engine/hub"

import (
	"context"
	"crypto/sha1"
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/hashicorp/go-getter"
	"gitlab.com/tromos/hub/coordinator"
	"gitlab.com/tromos/hub/device"
	"gitlab.com/tromos/hub/processor"
	"gitlab.com/tromos/hub/selector"
	"gitlab.com/tromos/tromos-ce/configuration"
	"gitlab.com/tromos/tromos-ce/pkg/beautify"
	"gitlab.com/tromos/tromos-ce/pkg/path"
	"os"
	"os/exec"
	"path/filepath"
	"plugin"
)

var (
	hub *Hub
)

func init() {
	h, err := NewHub(configuration.Env.GetString("TromosHub"))
	if err != nil {
		panic(err)
	}
	hub = h
}

// GetHub returns the default Hub
func GetHub() *Hub {
	return hub
}

// Hub is a insitu hub, or repository,  of modules and schemas
type Hub struct {
	ctx    context.Context
	path   string
	logger *logrus.Entry
}

// NewHub creates a New hub
func NewHub(pluginDir string) (*Hub, error) {
	if err := path.CreateIfNotExists(pluginDir, true); err != nil {
		return nil, err
	}

	return &Hub{
		ctx:    context.Background(),
		path:   pluginDir,
		logger: logrus.WithField("module", "hub"),
	}, nil
}

// shortenPath returns a hash of the path name. It is primarily create hashes of the
// given names and maintain the binary plugins in a flat namespace
func (hub *Hub) shortenPath(name string) string {
	pluginID := fmt.Sprintf("%x", sha1.Sum([]byte(name)))

	return hub.path + "/plugins/" + pluginID
}

// FixDependencies makes sure that all the modules are locally available.
// Names ending in (.bin) are assumed to be proprietary self-contained binaries that are
// directly downloaded. Otherwise, the plugin source is downloaded and locally compiled
// to produce the plugin binary
func FixDependencies(names []string) error {
	for _, name := range names {

		pluginpath := hub.shortenPath(name)
		if _, err := os.Stat(pluginpath); err == nil {
			hub.logger.Debugf("Plugin %s already exists @ %s", name, pluginpath)
			// TODO: version checkking
			continue
		}

		suffix := filepath.Ext(name)

		if suffix == "bin" {
			if err := hub.GetPluginBinary(name); err != nil {
				return err
			}
		}
		if err := hub.GetPluginSource(name); err != nil {
			return err
		}

	}
	return nil
}

// GetPluginSource calls GetPluginSource method on the default Hub
func GetPluginSource(name string) error { return hub.GetPluginSource(name) }

// GetPluginSource uses the go get command to download and compiles the plugin
// The binary will we stored in $TromosHUB/bin
func (hub *Hub) GetPluginSource(name string) error {
	pluginpath := hub.shortenPath(name)
	cmd := exec.Command("go", "build", "-o", pluginpath, "-buildmode=plugin", name)

	/*
	 TODO: link the stdout and stderr to the respective logrus outputs.
	 Until then, omit the command's output
	 */
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	hub.logger.Infof("Compile plugin %s -> %s", name, pluginpath)
	return cmd.Run()
}

// GetPluginBinary calls GetPluginBinary method on the default Hub
func GetPluginBinary(name string) error { return hub.GetPluginBinary(name) }

// GetPluginBinary downloads the binary plugin
func (hub *Hub) GetPluginBinary(name string) error {
	opts := []getter.ClientOption{getter.WithProgress(&beautify.ProgressBar{})}

	// release resources if get operation completes before time elapses
	ctx, cancel := context.WithTimeout(hub.ctx, configuration.Env.GetDuration("GetBinaryPluginTimeout"))
	defer cancel()

	client := &getter.Client{
		Ctx:     ctx,
		Src:     name,
		Dst:     hub.shortenPath(name),
		Mode:    getter.ClientModeFile,
		Options: opts,
	}

	if err := client.Get(); err != nil {
		hub.logger.WithError(err).Warnf("Download error %s", hub.shortenPath(name))
		return err
	}
	hub.logger.Infof("%s succesfully downloaded @ %s", name, hub.shortenPath(name))
	return nil
}

// OpenPlugin calls OpenPlugin method on the default Hub
func OpenPlugin(name string) (interface{}, error) { return hub.OpenPlugin(name) }

// OpenPlugin opens a plugin.
func (hub *Hub) OpenPlugin(name string) (interface{}, error) {
	binary := hub.shortenPath(name)

	hub.logger.Debugf("Open plugin %s from %s", name, binary)
	table, err := plugin.Open(binary)
	if err != nil {
		return nil, err
	}

	symbol, err := table.Lookup("Plugin")
	if err != nil {
		return nil, err
	}

	return symbol, nil
}

// OpenDevicePlugin calls OpenDevicePlugin method on the default Hub
func OpenDevicePlugin(name string) (device.Plugin, error) {
	return hub.OpenDevicePlugin(name)
}

// OpenDevicePlugin opens a Device plugin
func (hub *Hub) OpenDevicePlugin(name string) (device.Plugin, error) {
	if name == "" {
		return nil, ErrNotFound
	}

	symbol, err := hub.OpenPlugin(name)
	if err != nil {
		return nil, err
	}
	return *symbol.(*device.Plugin), nil
}

// OpenCoordinatorPlugin calls OpenCoordinatorPlugin method on the default Hub
func OpenCoordinatorPlugin(name string) (coordinator.Plugin, error) {
	return hub.OpenCoordinatorPlugin(name)
}

// OpenCoordinatorPlugin opens a Coordinator plugin
func (hub *Hub) OpenCoordinatorPlugin(name string) (coordinator.Plugin, error) {
	if name == "" {
		return nil, ErrNotFound
	}

	symbol, err := hub.OpenPlugin(name)
	if err != nil {
		return nil, err
	}
	return *symbol.(*coordinator.Plugin), nil
}

// OpenProcessorPlugin calls OpenProcessorPlugin method on the default Hub
func OpenProcessorPlugin(name string) (processor.Plugin, error) {
	return hub.OpenProcessorPlugin(name)
}

// OpenCoordinatorPlugin opens a Coordinator plugin
func (hub *Hub) OpenProcessorPlugin(name string) (processor.Plugin, error) {
	if name == "" {
		return nil, ErrNotFound
	}

	symbol, err := hub.OpenPlugin(name)
	if err != nil {
		return nil, err
	}

	return *symbol.(*processor.Plugin), nil
}

// OpenSelectorPlugin calls OpenSelectorPlugin method on the default Hub
func OpenSelectorPlugin(name string) (selector.Plugin, error) {
	return hub.OpenSelectorPlugin(name)
}

// OpenSelectorPlugin opens a Selector plugin
func (hub *Hub) OpenSelectorPlugin(name string) (selector.Plugin, error) {
	if name == "" {
		return nil, ErrNotFound
	}

	symbol, err := hub.OpenPlugin(name)
	if err != nil {
		return nil, err
	}
	return *symbol.(*selector.Plugin), nil
}
