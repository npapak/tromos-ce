// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package hub // import "gitlab.com/tromos/tromos-ce/engine/hub"

import (
	rpc "github.com/hprose/hprose-golang/rpc/websocket"
	"io/ioutil"
	"net/http"
	"os"
)

var (
	Service = ":7777"
)

func (hub *Hub) getPlugin(name string) ([]byte, error) {
	hub.logger.Debugf("Download request %s", name)

	// [1:] is to ommit the first start of the url
	pluginpath := hub.shortenPath(name)

	//Check if file exists and open
	file, err := os.Open(pluginpath)
	if err != nil {
		hub.logger.WithError(err).Warnf("Open error %s", name)
		return nil, err
	}

	defer hub.logger.Infof("Plugin %s succesfully downloaded", pluginpath)

	if err := file.Close(); err != nil {
		return nil, err
	}

	return ioutil.ReadFile(pluginpath)
}

// RunDistributionService calls RunDistributionService method on the default Hub
func RunDistributionService() {
	hub.RunDistributionService()
}

// RunDistributionService runs a sever in the default hub for distributing
// binary plugins to the daemons.
func (hub *Hub) RunDistributionService() {

	ops := rpc.NewWebSocketService()
	ops.AddFunction("Getplugin", hub.getPlugin)

	webservice := &http.Server{
		Addr:    Service,
		Handler: ops,
	}

	if err := webservice.ListenAndServe(); err != nil {
		hub.logger.WithError(err).Fatal("Webservice error")
	}

}

type WebServiceOperations struct {
	GetPlugin func(name string) ([]byte, error)
}

func (hub *Hub) GetPluginFromDistributionService(name string) error {

	// otherwise start a new instance
	call := &WebServiceOperations{}
	conn := rpc.NewWebSocketClient("ws://" + Service + "/")
	conn.UseService(call)

	pluginBin, err := call.GetPlugin(name)
	if err != nil {
		return err
	}

	//	hub.shortenPath(name)

	return ioutil.WriteFile("/tmp/poutses", pluginBin, 0644)
}
