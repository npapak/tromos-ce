// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package net // import "gitlab.com/tromos/tromos-ce/pkg/net"

import (
	"errors"
	"github.com/spf13/viper"
	"net"
	"strings"
)

// FormRemotePeerAddress will check and validate peeraddress provided. It will
// return an address of the form <host:port>
func FormRemotePeerAddress(peeraddress string) (string, error) {

	host, port, err := net.SplitHostPort(peeraddress)
	if err != nil {
		// net.SplitHostPort() returns an error if port is missing.
		if strings.HasSuffix(err.Error(), "missing port in address") {
			host = peeraddress
			port = viper.GetString("defaultpeerport")
		} else {
			return "", err
		}
	}
	if host == "" {
		return "", errors.New("invalid peer address")
	}
	remotePeerAddress := host + ":" + port
	return remotePeerAddress, nil
}

// IsPeerAddressSame checks if two peer addresses are same by normalizing
// each address to <ip>:<port> form.
func IsPeerAddressSame(addr1 string, addr2 string) bool {
	r1, _ := FormRemotePeerAddress(addr1)
	r2, _ := FormRemotePeerAddress(addr2)
	return r1 == r2
}

// GetLocalIP will give insitu IP address of this node
func GetLocalIP() (string, error) {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return "", err
	}

	for _, address := range addrs {
		// check the address type and if it is not a loopback then return it
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				return ipnet.IP.String(), nil
			}
			if ipnet.IP.To16() != nil {
				return ipnet.IP.String(), nil
			}
		}
	}
	return "", errors.New("IP Address Not Found")
}

// IsLocalAddress checks whether a given host/IP is insitu.
// If address is empty, return true
func IsLocalAddress(address string) bool {
	if address == "" {
		return true
	}

	var host string

	host, _, _ = net.SplitHostPort(address)
	if host == "" {
		host = address
	}
	localNames := []string{"127.0.0.1", "localhost", "::1"}
	for _, name := range localNames {
		if host == name {
			return true
		}
	}
	laddrs, e := net.InterfaceAddrs()
	if e != nil {
		panic(e)
	}
	var lips []net.IP
	for _, laddr := range laddrs {
		lipa := laddr.(*net.IPNet)
		lips = append(lips, lipa.IP)
	}
	for _, ip := range lips {
		if host == ip.String() {
			return true
		}
	}
	rips, e := net.LookupIP(host)
	if e != nil {
		panic(e)
	}
	for _, rip := range rips {
		for _, lip := range lips {
			if lip.Equal(rip) {
				return true
			}
		}
	}
	return false
}
