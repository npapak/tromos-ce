// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package path // import "gitlab.com/tromos/tromos-ce/pkg/path"

import (
	//	"github.com/docker/docker/pkg/idtools"
	//"golang.org/x/sys/unix"
	"os"
	"path/filepath"
)

const FilePathSeparator = string(filepath.Separator)

// Handle some relative paths
func Normalize(path string) string {
	path = filepath.Clean(path)

	switch path {
	case ".":
		return FilePathSeparator
	case "..":
		return FilePathSeparator
	case "*":
		return ""
	default:
		return path
	}
}

func Exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}

// CreateIfNotExists creates a file or a directory only if it does not already exist.
func CreateIfNotExists(path string, isDir bool) error {
	if _, err := os.Stat(path); err != nil {
		if os.IsNotExist(err) {
			if isDir {
				return os.MkdirAll(path, 0755)
			}
			if err := os.MkdirAll(filepath.Dir(path), 0755); err != nil {
				return err
			}
			f, err := os.OpenFile(path, os.O_CREATE, 0755)
			if err != nil {
				return err
			}
			f.Close()
		}
	}
	return nil
}

// Cleanup makes sure to provide a new directory free of stale data
func Cleanup(path string) error {
	/*
		if err := unix.Access(path, unix.W_OK); err != nil {
			return err
		}
	*/
	err := os.Remove(path)
	if os.IsNotExist(err) {
		return nil
	}
	if err != nil {
		return err
	}
	return nil
}

/*
// PrepareDir prepares and returns the default directory to for temporary files.
// If it doesn't exist, it is created. If it exists, its content is removed.
func PrepareDir(rootDir string, subdir string) (string, error) {
	idMapping, err := idtools.NewIdentityMapping("tromos", "tromos")
	if err != nil {
		return "", err
	}

	rootIdentity := idMapping.RootPair()

	var tmpDir string
	tmpDir = filepath.Join(rootDir, subdir)
	newName := tmpDir + "-old"
	if err := os.Rename(tmpDir, newName); err == nil {
		go func() {
			if err := os.RemoveAll(newName); err != nil {
				panic(err)
			}
		}()
	} else if !os.IsNotExist(err) {
		if err := os.RemoveAll(tmpDir); err != nil {
			return "", err
		}
	}

	// We don't remove the content of tmpdir if it's not the default,
	// it may hold things that do not belong to us.
	return tmpDir, os.MkdirAll(tmpDir, 0700)
}
*/
