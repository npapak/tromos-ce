// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package augmented // import "gitlab.com/tromos/tromos-ce/pkg/logical/augmented"

import (
	"encoding/binary"
	"fmt"
	"github.com/workiva/go-datastructures/augmentedtree"
)

type dimension struct {
	low, high int64
}

type interval struct {
	dimensions []*dimension
	id         uint64
}

func (i *interval) CheckDimension(dimension uint64) {
	if dimension > uint64(len(i.dimensions)) {
		panic(fmt.Sprintf(`Dimension :%d out of range.`, dimension))
	}
}

func (i *interval) LowAtDimension(dimension uint64) int64 {
	return i.dimensions[dimension-1].low
}

func (i *interval) HighAtDimension(dimension uint64) int64 {
	return i.dimensions[dimension-1].high
}

/* Partial overlap */
func (i *interval) OverlapsAtDimension(iv augmentedtree.Interval, dimension uint64) bool {
	return i.HighAtDimension(dimension) >= iv.LowAtDimension(dimension) &&
		i.LowAtDimension(dimension) <= iv.HighAtDimension(dimension)
}

func (i *interval) ID() uint64 {
	return i.id
}

func (i *interval) Key() []byte {
	key := make([]byte, 8)
	binary.LittleEndian.PutUint64(key, i.id)
	return key
}

func constructSingleDimension(low, high int64, id uint64) *interval {
	return &interval{[]*dimension{{low: low, high: high}}, id}
}
