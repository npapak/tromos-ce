// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package concurrency // import "gitlab.com/tromos/tromos-ce/pkg/concurrency"

import "sync/atomic"

// AtomicInteger is a int32 wrapper fo atomic
type AtomicInteger int32

// IncrementAndGet increment wrapped int32 with 1 and return new value.
func (i *AtomicInteger) IncrementAndGet() int32 {
	return atomic.AddInt32((*int32)(i), int32(1))
}

// GetAndIncrement increment wrapped int32 with 1 and return old value.
func (i *AtomicInteger) GetAndIncrement() int32 {
	ret := atomic.LoadInt32((*int32)(i))
	atomic.AddInt32((*int32)(i), int32(1))
	return ret
}

// DecrementAndGet decrement wrapped int32 with 1 and return new value.
func (i *AtomicInteger) DecrementAndGet() int32 {
	return atomic.AddInt32((*int32)(i), int32(-1))
}

// GetAndDecrement decrement wrapped int32 with 1 and return old value.
func (i *AtomicInteger) GetAndDecrement() int32 {
	ret := atomic.LoadInt32((*int32)(i))
	atomic.AddInt32((*int32)(i), int32(-1))
	return ret
}

// Get current value
func (i *AtomicInteger) Get() int32 {
	return atomic.LoadInt32((*int32)(i))
}
