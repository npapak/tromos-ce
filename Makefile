PROJECT_PATH := "gitlab.com/tromos"
PROJECT_NAME := "tromos-ce"
PKG := "$(PROJECT_PATH)/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

#FLAGS := -race

.PHONY: all
all: daemon tromos-cli hub


.PHONY: dep
dep: ## Get the dependencies
	@go mod tidy

.PHONY: pre-release-check
pre-release-check: # perform release checks (fmt, linting)
	## Beatify and simplify the code
	@go list -f {{.Dir}} ./... | xargs gofmt -w -s
	## examines source code and reports suspicious constructs
	@golangci-lint run


.PHONY: devtools
devel-deps: ## Get additional development dependencies
	# binary will be $(go env GOPATH)/bin/golangci-lint
	@curl -sfL https://install.goreleaser.com/github.com/golangci/golangci-lint.sh | sh -s -- -b $(shell go env GOPATH)/bin 
	@go get \
	github.com/pierrre/gotestcover 	\
	github.com/mattn/goveralls



.PHONY: test
test: ## Run unittests
	@go test ${FLAGS} -short ${PKG_LIST}


.PHONY: coverage
coverage: ## Generate global code coverage report
	@gotestcover -v -short -covermode=count -coverprofile=.profile.cov -parallelpackages=4 ./...


.PHONY: tromos-daemon
daemon: ## Build the binary for tromos daemon
	@echo "===> Compile Daemon"
	@go build -o tromos-daemon ${FLAGS} -v gitlab.com/tromos/tromos-ce/cmd/tromos-daemon


.PHONY: tromos-cli
tromos-cli: ## Build the binary for tromos
	@echo "===> Compile Tromos-cli"
	@go build -o tromos-cli ${FLAGS} -v gitlab.com/tromos/tromos-ce/cmd/tromos-cli


.PHONY: clean
clean: ## Remove previous build
	@go clean
	@rm -f $(PROJECT_NAME)


# Absolutely awesome: http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help
help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := help
