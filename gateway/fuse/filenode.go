// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package tromosfs // import "gitlab.com/tromos/tromos-ce/gateway/fuse"

import (
	"bazil.org/fuse"
	"bazil.org/fuse/fs"
	"context"
	"gitlab.com/tromos/tromos-ce/engine/middleware"
	"gitlab.com/tromos/tromos-ce/pkg/debug"
	"gitlab.com/tromos/tromos-ce/pkg/log"
	"gitlab.com/tromos/tromos-ce/pkg/logical/augmented"
	"os"
)

type FileNode struct {
	name    string
	handler *File
}

// return made-up attributes instead of maintaining the state
func (fi *FileNode) Attr(ctx context.Context, a *fuse.Attr) error {
	log.Trace("-> ", debug.WhereAmI())
	defer log.Trace("<- ", debug.WhereAmI())

	a.Mode = os.FileMode(0666)
	a.Size = uint64(fi.handler.logical.Length())
	a.Uid = uint32(os.Getuid())
	a.Gid = uint32(os.Getgid())
	return nil
}

func (fi *FileNode) Setattr(ctx context.Context, req *fuse.SetattrRequest, resp *fuse.SetattrResponse) error {
	log.Trace("-> ", debug.WhereAmI())
	defer log.Trace("<- ", debug.WhereAmI())

	if req.Valid.Size() {
		// An easy but expensive way to implement it is to get the data
		// in a insitu snapshot, chop it, and write the snapshot back to the system
		log.Admin("Truncate ", fi.handler.logical.Length(), " to ", req.Size, " value has only insitu effect")

		c := block{
			offset: 0,
			data:   make([]byte, req.Size),
		}
		index := fi.handler.logical.Add(0, int64(req.Size))
		fi.handler.cached[index] = c
		// We do not add it to dirties, as we do not want to flush it
	}
	return nil
}

func (fi *FileNode) Open(ctx context.Context, req *fuse.OpenRequest, resp *fuse.OpenResponse) (fs.Handle, error) {
	return fi.open(req.Flags, resp)
}

func (fi *FileNode) open(flags fuse.OpenFlags, resp *fuse.OpenResponse) (fs.Handle, error) {
	log.Trace("-> ", debug.WhereAmI())
	defer log.Trace("<- ", debug.WhereAmI())

	switch {
	case flags.IsReadOnly():
		log.User("Open rdonly:", flags)
		if flags&fuse.OpenTruncate != 0 {
			log.Admin("tried to open a readonly file with truncate")
			return nil, fuse.ENOTSUP
		}
		if flags&fuse.OpenAppend != 0 {
			log.Admin("tried to open a readonly file with append")
			return nil, fuse.ENOTSUP
		}

		rtx, err := storage.BeginTx(fi.name, middleware.RDONLY)
		if err != nil {
			if err := rtx.Rollback(); err != nil {
				panic(err)
			}
			return nil, MaskError(err)
		}

		tree := augmented.NewTree(0, 0)
		rtx.Cursor().ForEach(func(stx *middleware.SubTx) bool {
			tree.Add(int64(stx.Offset), int64(stx.Size))
			return true
		})

		file := &File{
			logical: tree,
			reader:  rtx,
		}
		file.init()
		fi.handler = file

		resp.Flags = fuse.OpenDirectIO //| fuse.OpenKeepsnapshot
		return file, nil

	case flags.IsWriteOnly():
		log.User("Open wronly:", flags)
		if flags&fuse.OpenTruncate != 0 {
			err := storage.Truncate(fi.name)
			if err != nil {
				return nil, MaskError(err)
			}
		}

		wtx, err := storage.BeginTx(fi.name, middleware.WONLY)
		if err != nil {
			return nil, MaskError(err)
		}

		file := &File{
			logical: augmented.NewTree(0, 0),
			writer:  wtx,
		}
		file.init()
		fi.handler = file

		if flags&fuse.OpenAppend != 0 {
			// Find here and keep the offset. all the following
			// writes will be from that offset
			log.Admin("ToDO: open for append ")
		}

		resp.Flags = fuse.OpenDirectIO //| fuse.OpenKeepsnapshot
		return file, nil

	case flags.IsReadWrite():
		log.Admin("Open rw:", flags, " file:", fi.name)
		if flags&fuse.OpenTruncate != 0 {
			log.Admin("Truncate")
			if err := storage.Truncate(fi.name); err != nil {
				return nil, MaskError(err)
			}
		}

		rtx, err := storage.BeginTx(fi.name, middleware.RDONLY)
		if err != nil {
			if err := rtx.Rollback(); err != nil {
				panic(err)
			}
			return nil, MaskError(err)
		}

		tree := augmented.NewTree(0, 0)

		rtx.Cursor().ForEach(func(stx *middleware.SubTx) bool {
			tree.Add(int64(stx.Offset), int64(stx.Size))
			return true
		})

		wtx, err := storage.BeginTx(fi.name, middleware.WONLY)
		if err != nil {
			if err := wtx.Rollback(); err != nil {
				panic(err)
			}
			return nil, MaskError(err)
		}

		file := &File{
			logical: tree,
			writer:  wtx,
			reader:  rtx,
		}
		file.init()
		fi.handler = file
		resp.Flags = fuse.OpenDirectIO //| fuse.OpenKeepsnapshot
		return file, nil

	default:
		return nil, fuse.ESTALE
	}
}

func (fi *FileNode) Fsync(ctx context.Context, req *fuse.FsyncRequest) error {
	log.Trace("-> ", debug.WhereAmI())
	defer log.Trace("<- ", debug.WhereAmI())

	// Normally it dictates which handler of the filenode to fsync
	// <- Fsync [ID=0xc Node=0x2 Uid=1001 Gid=1001 Pid=25558] Handle 0x1 Flags 0
	// For example this fsync will sync the handler 0x01
	return nil
}
