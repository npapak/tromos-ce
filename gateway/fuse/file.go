// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package tromosfs // import "gitlab.com/tromos/tromos-ce/gateway/fuse"

import (
	"bazil.org/fuse"
	"context"
	"gitlab.com/tromos/tromos-ce/engine/middleware"
	"gitlab.com/tromos/tromos-ce/pkg/debug"
	"gitlab.com/tromos/tromos-ce/pkg/log"
	"gitlab.com/tromos/tromos-ce/pkg/logical/augmented"
	"io"
)

type block struct {
	offset int64 // Offset in the logical contiguous space
	data   []byte
}

type File struct {
	writer *middleware.Tx
	reader *middleware.Tx
	closed bool

	logical *augmented.Tree // Resolve what changes should be presented on read
	cached  map[uint64]block
	dirty   []uint64
}

func (f *File) init() {
	f.cached = make(map[uint64]block)
}

func (f *File) Read(ctx context.Context, req *fuse.ReadRequest, resp *fuse.ReadResponse) error {
	log.Trace("-> ", debug.WhereAmI())
	defer log.Trace("<- ", debug.WhereAmI())

	if f.closed {
		log.Admin("File is already closed")
		return fuse.EIO
	}

	// Make sure it will not exceed the filesize
	if req.Offset+int64(req.Size) > f.logical.Length() {
		req.Size = int(f.logical.Length() - req.Offset)
		if req.Size == 0 {
			// EOF
			return nil
		}
	}

	var wb int
	// Bring data into memory, if they are not already there
	_, segments := f.logical.Overlaps(req.Offset, int64(req.Size))
	for _, segment := range segments {
		page, ok := f.cached[segment.Index]
		if !ok {
			from, data, err := f.reader.Get(segment.Index)
			if err != nil {
				return MaskError(err)
			}

			page = block{
				offset: int64(from),
				data:   data,
			}
			f.cached[segment.Index] = page
		}
		from := int64(req.Offset) + int64(wb) - segment.From

		// Start filling the reply with data
		wb += copy(resp.Data[len(resp.Data):req.Size], page.data[from:])
		resp.Data = resp.Data[:wb]
	}
	return nil
}

func (f *File) Write(ctx context.Context, req *fuse.WriteRequest, resp *fuse.WriteResponse) error {
	log.Trace("-> ", debug.WhereAmI())
	defer log.Trace("<- ", debug.WhereAmI())

	if f.closed {
		log.Admin("File is already closed")
		return fuse.EIO
	}

	oflags := fuse.OpenFlags(req.FileFlags)
	switch {
	case oflags.IsReadOnly():
		return fuse.EPERM

	case oflags.IsWriteOnly():

		// There is no read, so stream data directly to the backend
		wb, err := f.writer.Update(req.Offset, func(pw io.ReadWriteCloser) (int, error) {
			wb, err := pw.Write(req.Data)
			return wb, err
		})

		if err != nil {
			return MaskError(err)
		}
		resp.Size = wb

	case oflags.IsReadWrite():
		// Because a client may later read back the data it is
		// prefferable to keep them insitu and write them on flush
		c := block{
			offset: req.Offset,
			data:   make([]byte, len(req.Data)),
		}
		resp.Size = copy(c.data, req.Data)

		index := f.logical.Add(req.Offset, int64(resp.Size))
		f.cached[index] = c
		f.dirty = append(f.dirty, index)
	default:
		return fuse.ENOSYS
	}
	return nil
}

/*
func (f *File) Flush(ctx context.Context, req *fuse.FlushRequest) error {
	log.Trace("-> ", debug.WhereAmI())
	defer log.Trace("<- ", debug.WhereAmI())
}
*/

func (f *File) Release(ctx context.Context, req *fuse.ReleaseRequest) error {
	log.Trace("-> ", debug.WhereAmI())
	defer log.Trace("<- ", debug.WhereAmI())

	if f.closed {
		log.Admin("File is already closed")
		return fuse.EIO
	}
	f.closed = true

	// RDONLY does not have writers
	if f.writer != nil {
		for _, index := range f.dirty {

			_, err := f.writer.Update(f.cached[index].offset, func(pw io.ReadWriteCloser) (int, error) {
				wb, err := pw.Write(f.cached[index].data)
				return wb, err
			})

			if err != nil {
				Check(f.writer.Rollback())
				return MaskError(err)
			}
		}

		if err := f.writer.Commit(); err != nil {
			return MaskError(err)
		}
	}

	// First wait for any changes to be persisted and then release the cached data
	if f.cached != nil {
		f.cached = nil
	}

	if f.reader != nil {
		if err := f.reader.Rollback(); err != nil {
			return err
		}
	}

	f.dirty = nil
	return nil
}

func (f *File) Forget() {
	log.Trace("-> ", debug.WhereAmI())
	defer log.Trace("<- ", debug.WhereAmI())
}
