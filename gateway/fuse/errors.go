// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package tromosfs // import "gitlab.com/tromos/tromos-ce/gateway/fuse"

import (
	"bazil.org/fuse"
	"gitlab.com/tromos/hub/coordinator"
	"gitlab.com/tromos/tromos-ce/pkg/log"
)

func Check(err error) {
	if err != nil {
		panic(err)
	}
	return
}

// Convert Tromos errors to Fuse errors
func MaskError(err error) error {
	switch err {
	case nil:
		return nil

	case coordinator.ErrBackend:
		return fuse.ENOSYS

	case coordinator.ErrNoImpl:
		return fuse.ENOSYS

	case coordinator.ErrKey:
		return fuse.EIO

	case coordinator.ErrNoExist:
		return fuse.ENOENT

	default:
		log.Admin("Uncaptured error:", err)
		return err
	}
}
