// Licensed to Fotis Nikolaidis (nikolaidis.fotis@gmail.com) under one or more contributor
// license agreements. See the NOTICE file distributed with
// this work for additional information regarding copyright
// ownership. Fotis Nikolaidis (nikolaidis.fotis@gmail.com) licenses this file to you under
// the Apache License, Version 2.0 (the "License"); you may
// not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package tromosfs // import "gitlab.com/tromos/tromos-ce/gateway/fuse"

import (
	"bazil.org/fuse"
	"bazil.org/fuse/fs"
	"context"
	"gitlab.com/tromos/tromos-ce/engine/middleware"
	"gitlab.com/tromos/tromos-ce/pkg/debug"
	"gitlab.com/tromos/tromos-ce/pkg/log"
	"gitlab.com/tromos/tromos-ce/pkg/path"
	"os"
)

type Directory struct {
	name     string
	children map[string]interface{}
}

// Attr returns the attributes of a given node.
func (dir *Directory) Attr(ctx context.Context, a *fuse.Attr) error {
	a.Mode = os.ModeDir | 0555
	a.Uid = uint32(os.Getuid())
	a.Gid = uint32(os.Getgid())
	return nil
}

func (dir *Directory) Lookup(ctx context.Context, name string) (fs.Node, error) {
	log.Trace("-> ", debug.WhereAmI())
	defer log.Trace("<- ", debug.WhereAmI())

	name = path.Normalize(name)

	// First check it if is a insitu node
	child, ok := dir.children[name]
	if !ok {
		// If it is not, return its projection
		_, err := storage.BeginTx(name, middleware.VIEW)
		err = MaskError(err)
		if err != nil {
			return nil, err
		}
		return &FileNode{name: name}, nil
	}

	switch child.(type) {
	case *Directory:
		return child.(*Directory), nil
	case *FileNode:
		return child.(*FileNode), nil
	default:
		// NB: if this happens, we do not want to continue,
		// unpredictable behaviour
		// may occur.
		panic("invalid type found under directory. programmer error.")
	}
}

func (dir *Directory) ReadDirAll(ctx context.Context) ([]fuse.Dirent, error) {
	log.Trace("-> ", debug.WhereAmI())
	defer log.Trace("<- ", debug.WhereAmI())

	dirList := make([]fuse.Dirent, 0, len(dir.children))
	for key := range dir.children {
		dirList = append(dirList, fuse.Dirent{
			Type: fuse.DT_File,
			Name: key,
		})
	}
	return dirList, nil
}

func (dir *Directory) Mkdir(ctx context.Context, req *fuse.MkdirRequest) (fs.Node, error) {
	log.Trace("-> ", debug.WhereAmI())
	defer log.Trace("<- ", debug.WhereAmI())

	req.Name = path.Normalize(req.Name)

	newDir := &Directory{name: req.Name, children: make(map[string]interface{})}
	// TODO: Reflect it to trio
	dir.children[req.Name] = newDir
	return newDir, nil
}

func (dir *Directory) Rename(ctx context.Context, req *fuse.RenameRequest, newDir fs.Node) error {
	log.Trace("-> ", debug.WhereAmI())
	defer log.Trace("<- ", debug.WhereAmI())

	req.NewName = path.Normalize(req.NewName)
	req.OldName = path.Normalize(req.OldName)

	if req.NewName == req.OldName {
		return nil
	}

	_, ok := dir.children[req.NewName]
	if ok {
		return fuse.EEXIST
	}
	_, ok = dir.children[req.OldName]
	if !ok {
		return fuse.ENOENT
	}

	dir.children[req.NewName] = dir
	delete(dir.children, req.OldName)
	return nil
}

func (dir *Directory) Create(ctx context.Context, req *fuse.CreateRequest, resp *fuse.CreateResponse) (fs.Node, fs.Handle, error) {
	log.Trace("-> ", debug.WhereAmI())
	defer log.Trace("<- ", debug.WhereAmI())

	req.Name = path.Normalize(req.Name)

	// New "empty" file
	if err := storage.CreateOrReset(req.Name); err != nil {
		return nil, nil, MaskError(err)
	}

	// Project it to the filesyste,
	filenode := &FileNode{name: req.Name}
	file, err := filenode.open(req.Flags, &resp.OpenResponse)
	if err != nil {
		return nil, nil, MaskError(err)
	}

	// On success register the child
	dir.children[req.Name] = filenode

	return filenode, file, nil
}

func (dir *Directory) Remove(ctx context.Context, req *fuse.RemoveRequest) error {
	log.Trace("-> ", debug.WhereAmI())
	defer log.Trace("<- ", debug.WhereAmI())

	req.Name = path.Normalize(req.Name)

	_, ok := dir.children[req.Name]
	if !ok {
		return nil
	}

	if err := storage.Remove(req.Name); err != nil {
		return MaskError(err)
	}
	delete(dir.children, req.Name)
	return nil
}
